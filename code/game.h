/*
File:   game.h
Author: Taylor Robbins
Date:   03\03\2018
*/

#ifndef _GAME_H
#define _GAME_H

struct GameData_t
{
	bool initialized;
	
	Texture_t testTexture;
	Texture_t circuitTexture;
	Texture_t missingTexture;
	Texture_t canvasTexture;
	
	u32 scoreLeft;
	u32 scoreRight;
	u8 potPointsLeft;
	u8 potPointsRight;
	r32 panelsAnimAmount;
	r32 answerAnimAmount;
	r32 fourPointAnimAmount;
	u32 fourPointTimeElapsed;
	r32 clueBoardAnimAmount;
	bool clueRevealed[R2_NUM_CLUES_PER_Q];
	r32 clueAnimAmounts[R2_NUM_CLUES_PER_Q];
	bool showCountdown;
	r32 countdownAnimAmount;
	u32 countdownTimeLeft;
	u32 countdownTimeRight;
	bool rightTeamWins;
	u32 timeTillClue;
	r32 winTextAnimAmount;
	
	u64 volumeChangeTime;
	SoundInstance_t* questionMusic;
	SoundInstance_t* countdownMusic;
	SoundInstance_t* finishSound;
	SoundInstance_t* finishMusic;
	
	u64 buttonLeftLastPress;
	u64 buttonRightLastPress;
	
	// Round State
	u8 roundNum; //0-3
	u8 questionNum;
	u8 numCluesShown;
	u8 buttonTriggered;
	bool switchedSides;
	bool rightTeamTurn;
	bool showQuestion;
	bool timerPaused;
	bool answeredCorrectly;
	bool showAnswer;
	bool showBoard;
	
	//Round 1
	char* r1Questions[R1_NUM_QUESTIONS];
	char* r1Clues[R1_NUM_QUESTIONS*R1_NUM_CLUES_PER_Q];
	char* r1Answers[R1_NUM_QUESTIONS];
	
	//Round 2
	char* r2Answers[R2_NUM_QUESTIONS];
	char* r2Clues[R2_NUM_QUESTIONS*R2_NUM_CLUES_PER_Q];
	
	//Round 3
	u32 r3NumQuestions;
	char** r3Questions;
	char** r3Answers;
	char** r3Clues;
	
	//Tray
	rec trayRec;
	rec questionRec;
	
	rec pointStackLeftRec;
	rec pointLeftBaseRec;
	rec scoreLeftRec;
	
	rec pointStackRightRec;
	rec pointRightBaseRec;
	rec scoreRightRec;
	
	v2 lateralLogoPos;
	
	v2 questionTextPos;
	const char* questionStr;
	const char* answerStr;
	FontFlowInfo_t questionFlow;
	
	v2 scoreLeftTextPos1;
	v2 scoreLeftTextPos2;
	FontFlowInfo_t scoreLeftFlow;
	
	v2 scoreRightTextPos1;
	v2 scoreRightTextPos2;
	FontFlowInfo_t scoreRightFlow;
	
	//Four Point Slider
	rec fourPointRec;
	r32 fourPointSliderAmount;
	rec fourPointSliderRec;
	v2  fourPointTextPos[4];
	rec fourPointPieceRecs[4];
	
	//Clue Board
	rec clueBoardRec;
	
	rec clueRecs[R2_NUM_CLUES_PER_Q];
	rec clueCoverRecs[R2_NUM_CLUES_PER_Q];
	v2  clueNumberTextPos[R2_NUM_CLUES_PER_Q];
	
	v2 clueTextPos[R2_NUM_CLUES_PER_Q];
	FontFlowInfo_t clueFlows[R2_NUM_CLUES_PER_Q];
};

#endif //  _GAME_H
