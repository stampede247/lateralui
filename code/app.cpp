/*
File:   app.cpp
Author: Taylor Robbins
Date:   03\02\2018
Description: 
	** Holds the top-level functions that are exported in the application DLL that gets loaded by the platform layer
	** This is the only file that really gets compiled for the application. All other files are included inside this one
*/

#define RESET_APPLICATION false
#define USE_ASSERT_FAILURE_FUNCTION true

#include "platformInterface.h"
#include "app_version.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_RECT_PACK_IMPLEMENTATION
#include "stb_rect_pack.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include "my_tempMemory.h"
#include "my_tempMemory.cpp"
#include "my_tempList.h"

// +--------------------------------------------------------------+
// |                   Application Header Files                   |
// +--------------------------------------------------------------+
#include "app_defines.h"
#include "app_structs.h"
#include "app_renderContext.h"
#include "app_dynamicColor.h"
#include "app_textBox.h"

#include "game.h"
#include "setup.h"
#include "app_data.h"

// +--------------------------------------------------------------+
// |                     Application Globals                      |
// +--------------------------------------------------------------+
const PlatformInfo_t* platform = nullptr;
const AppInput_t* input = nullptr;
AppOutput_t* appOutput = nullptr;

AppData_t* app = nullptr;
GameData_t* game = nullptr;
SetupData_t* setup = nullptr;

MemoryArena_t* mainHeap = nullptr;
RenderContext_t* renderContext = nullptr;
v2 RenderScreenSize = Vec2_Zero;
v2 RenderMousePos = Vec2_Zero;
v2 RenderMouseStartLeft = Vec2_Zero;
v2 RenderMouseStartRight = Vec2_Zero;

// +--------------------------------------------------------------+
// |                      Application Macros                      |
// +--------------------------------------------------------------+
#define DEBUG_Write(formatStr) do {      \
	if (platform != nullptr &&           \
		platform->DebugWrite != nullptr) \
	{                                    \
		platform->DebugWrite(formatStr); \
	}                                    \
} while (0)

#define DEBUG_WriteLine(formatStr) do {      \
	if (platform != nullptr &&               \
		platform->DebugWriteLine != nullptr) \
	{                                        \
		platform->DebugWriteLine(formatStr); \
	}                                        \
} while (0)

#define DEBUG_Print(formatStr, ...) do {              \
	if (platform != nullptr &&                        \
		platform->DebugPrint != nullptr)              \
	{                                                 \
		platform->DebugPrint(formatStr, __VA_ARGS__); \
	}                                                 \
} while (0)

#define DEBUG_PrintLine(formatStr, ...) do {              \
	if (platform != nullptr &&                            \
		platform->DebugPrintLine != nullptr)              \
	{                                                     \
		platform->DebugPrintLine(formatStr, __VA_ARGS__); \
	}                                                     \
} while (0)

#define HandleButton(button) do { app->buttonHandled[button] = true; } while(0)
#define IsButtonHandled(button) app->buttonHandled[button]

#define ButtonPressed(button) ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonPressedUnhandled(button) (app->buttonHandled[button] == false && ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2))
#define ButtonReleased(button) ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonReleasedUnhandled(button) (app->buttonHandled[button] == false && ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2))
#define ButtonDown(button) (input->buttons[button].isDown)
#define ButtonDownUnhandled(button) (app->buttonHandled[button] == false && input->buttons[button].isDown)

#define ClickedOnRec(rectangle) (ButtonReleasedUnhandled(MouseButton_Left) && IsInsideRec(rectangle, RenderMousePos) && IsInsideRec(rectangle, RenderMouseStartLeft))
#define TimeSince(programTimeValue) ((platform->programTime >= programTimeValue) ? (platform->programTime - (programTimeValue)) : (UINT32_MAX - (programTimeValue) + platform->programTime))

void HandleTextBoxEnter(TextBox_t* textBoxPntr);

// +--------------------------------------------------------------+
// |                   Application Source Files                   |
// +--------------------------------------------------------------+
#include "app_performance.cpp"
#include "app_wav.cpp"
#include "app_sound.cpp"
#include "app_texture.cpp"
#include "app_font.cpp"
#include "app_loadingFunctions.cpp"
#include "app_fontHelpers.cpp"
#include "app_renderContext.cpp"
#include "app_textBox.cpp"
#include "app_fontFlow.cpp"

#include "app_helpers.cpp"
#include "game.cpp"
#include "setup.cpp"

// +--------------------------------------------------------------+
// |                    Local Helper Functions                    |
// +--------------------------------------------------------------+
void AppLoadContent(bool firstLoad)
{
	if (!firstLoad)
	{
		DestroyShader(&app->defaultShader);
		DestroyFont(&app->defaultFont);
		DestroyNewFont(&app->uiFont);
		DestroyNewFont(&app->scoreFont);
		DestroyNewFont(&app->questionFont);
		DestroyNewFont(&app->clueFont);
		DestroyNewFont(&app->clueNumberFont);
		DestroyNewFont(&app->debugFont);
		DestroyTexture(&app->turnSprite);
		DestroySound(&app->testSound);
		DestroySound(&app->round1Sound);
		DestroySound(&app->buttonSound);
		DestroySound(&app->countdownStartSound);
		DestroySound(&app->finalMinuteSound);
		DestroySound(&app->thinkingLoop1);
		DestroySound(&app->thinkingLoop2);
		DestroySound(&app->successSound);
		DestroySound(&app->failureSound);
		DestroySound(&app->finishSound);
		DestroySound(&app->finishSong);
	}
	
	app->turnSprite = LoadTexture(SPRITES_FOLDER "yourTurn.png", false, false);
	app->testSound = LoadSound(SOUNDS_FOLDER "test.wav");
	app->round1Sound = LoadSound(SOUNDS_FOLDER "round1.wav");
	app->buttonSound = LoadSound(SOUNDS_FOLDER "button1.wav");
	app->countdownStartSound = LoadSound(SOUNDS_FOLDER "countdownStart.wav");
	app->finalMinuteSound = LoadSound(SOUNDS_FOLDER "finalMinute.wav");
	app->thinkingLoop1 = LoadSound(SOUNDS_FOLDER "thinkingLoop1.wav");
	app->thinkingLoop2 = LoadSound(SOUNDS_FOLDER "thinkingLoop2.wav");
	app->successSound = LoadSound(SOUNDS_FOLDER "success.wav");
	app->failureSound = LoadSound(SOUNDS_FOLDER "failure.wav");
	app->finishSound = LoadSound(SOUNDS_FOLDER "gameFinishSound.wav");
	app->finishSong = LoadSound(SOUNDS_FOLDER "gameFinishSong.wav");
	
	app->defaultShader = LoadShader(SHADERS_FOLDER "simple-vertex.glsl", SHADERS_FOLDER "simple-fragment.glsl");
	app->defaultFont   = LoadFont(FONTS_FOLDER "georgiab.ttf", 24, 256, 256, ' ', 96);
	
	CreateGameFont(&app->uiFont, mainHeap);
	FontLoadFile(&app->uiFont, FONTS_FOLDER "LatoWeb-Light.ttf",  FontStyle_Default);
	FontLoadFile(&app->uiFont, FONTS_FOLDER "LatoWeb-Semibold.ttf", FontStyle_Bold);
	FontBake(&app->uiFont, UI_FONT_SIZE);
	FontBake(&app->uiFont, UI_FONT_SIZE, FontStyle_Bold);
	FontDropFiles(&app->uiFont);
	
	CreateGameFont(&app->scoreFont, mainHeap);
	FontLoadFile(&app->scoreFont, FONTS_FOLDER "Beantown.ttf", FontStyle_Default);
	FontBake(&app->scoreFont, SCORE_FONT_SIZE, FontStyle_Default, ' ', 96, 1024, 1024);
	FontDropFiles(&app->scoreFont);
	
	CreateGameFont(&app->questionFont, mainHeap);
	FontLoadFile(&app->questionFont, FONTS_FOLDER "LatoWeb-Regular.ttf",    FontStyle_Default);
	FontLoadFile(&app->questionFont, FONTS_FOLDER "LatoWeb-Italic.ttf",     FontStyle_Italic);
	FontLoadFile(&app->questionFont, FONTS_FOLDER "LatoWeb-Bold.ttf",       FontStyle_Bold);
	FontLoadFile(&app->questionFont, FONTS_FOLDER "LatoWeb-BoldItalic.ttf", FontStyle_BoldItalic);
	FontBake(&app->questionFont, QUESTION_FONT_SIZE, FontStyle_Default);
	FontBake(&app->questionFont, QUESTION_FONT_SIZE, FontStyle_Italic);
	FontBake(&app->questionFont, QUESTION_FONT_SIZE, FontStyle_Bold);
	FontBake(&app->questionFont, QUESTION_FONT_SIZE, FontStyle_BoldItalic);
	FontDropFiles(&app->questionFont);
	
	CreateGameFont(&app->clueFont, mainHeap);
	FontLoadFile(&app->clueFont, FONTS_FOLDER "LatoWeb-Regular.ttf",    FontStyle_Default);
	FontLoadFile(&app->clueFont, FONTS_FOLDER "LatoWeb-Italic.ttf",     FontStyle_Italic);
	FontLoadFile(&app->clueFont, FONTS_FOLDER "LatoWeb-Bold.ttf",       FontStyle_Bold);
	FontLoadFile(&app->clueFont, FONTS_FOLDER "LatoWeb-BoldItalic.ttf", FontStyle_BoldItalic);
	FontBake(&app->clueFont, CLUE_FONT_SIZE, FontStyle_Default);
	FontBake(&app->clueFont, CLUE_FONT_SIZE, FontStyle_Italic);
	FontBake(&app->clueFont, CLUE_FONT_SIZE, FontStyle_Bold);
	FontBake(&app->clueFont, CLUE_FONT_SIZE, FontStyle_BoldItalic);
	FontDropFiles(&app->clueFont);
	
	CreateGameFont(&app->clueNumberFont, mainHeap);
	FontLoadFile(&app->clueNumberFont, FONTS_FOLDER "LatoWeb-Bold.ttf", FontStyle_Default);
	FontBake(&app->clueNumberFont, CLUE_NUMBER_FONT_SIZE, FontStyle_Default, ' ', 96, 1024, 1024);
	FontDropFiles(&app->clueNumberFont);
	
	CreateGameFont(&app->debugFont, mainHeap);
	FontLoadFile(&app->debugFont, FONTS_FOLDER "consola.ttf", FontStyle_Default);
	FontBake(&app->debugFont, 12);
	FontBake(&app->debugFont, 16);
	FontBake(&app->debugFont, 20);
	FontBake(&app->debugFont, 24);
	FontDropFiles(&app->debugFont);
}

void AppProcessLine(const char* lineStr)
{
	u32 lineLength = (u32)strlen(lineStr);
	// while (lineLength > 0 && !IsCharClassPrintable(lineStr[0]))
	// {
	// 	// DEBUG_PrintLine("Removing %02X", lineStr[0]);
	// 	lineStr++;
	// 	lineLength--;
	// }
	
	if (lineLength > 0 && lineStr[0] == '~')
	{
		if (lineLength >= 5 && strncmp(lineStr, "~ping", 5) == 0)
		{
			u32 numStates = 0;
			u32* states = ParseNumbersInString(TempArena, NtStr(lineStr), &numStates);
			if (numStates == 4)
			{
				bool firstPing = false;
				if (!app->gotPortPing)
				{
					DEBUG_WriteLine("Got Port Ping!");
					app->gotPortPing = true;
					firstPing = true;
				}
				app->lastPortPing = platform->programTime;
				bool newButton1Connected = (states[0] != 0);
				bool newButton1Down      = (states[1] != 0);
				bool newButton2Connected = (states[2] != 0);
				bool newButton2Down      = (states[3] != 0);
				if (firstPing || app->button1Connected != newButton1Connected)
				{
					DEBUG_PrintLine("Button1 %s", newButton1Connected ? "Connected" : "Disconnected");
					app->button1Connected = newButton1Connected;
				}
				if (firstPing || app->button1Down != newButton1Down)
				{
					DEBUG_PrintLine("Button1 %s", newButton1Down ? "Pressed" : "Released");
					app->button1Down = newButton1Down;
				}
				if (firstPing || app->button2Connected != newButton2Connected)
				{
					DEBUG_PrintLine("Button2 %s", newButton2Connected ? "Connected" : "Disconnected");
					app->button2Connected = newButton2Connected;
				}
				if (firstPing || app->button2Down != newButton2Down)
				{
					DEBUG_PrintLine("Button2 %s", newButton2Down ? "Pressed" : "Released");
					app->button2Down = newButton2Down;
				}
			}
			else
			{
				DEBUG_PrintLine("Only %u numbers in ping command! \"%s\"", numStates, lineStr);
			}
		}
		else if (lineLength >= 6 && strncmp(lineStr, "~State", 6) == 0)
		{
			u32 numNumbers = 0;
			u32* numbers = ParseNumbersInString(TempArena, NtStr(lineStr), &numNumbers);
			if (numNumbers == 2)
			{
				DEBUG_PrintLine("Button%u %s!", numbers[0], (numbers[1] != 0) ? "Pressed" : "Released");
				if (numbers[0] == 1)
				{
					app->button1Down = (numbers[1] != 0);
				}
				else if (numbers[0] == 2)
				{
					app->button2Down = (numbers[1] != 0);
				}
			}
			else
			{
				DEBUG_PrintLine("Only %u numbers in State command! \"%s\"", numNumbers, lineStr);
			}
		}
		else if (lineLength >= 5 && strncmp(lineStr, "~Plug", 5) == 0)
		{
			u32 numNumbers = 0;
			u32* numbers = ParseNumbersInString(TempArena, NtStr(lineStr), &numNumbers);
			if (numNumbers == 2)
			{
				DEBUG_PrintLine("Button%u %s!", numbers[0], (numbers[1] != 0) ? "Connected" : "Disconnected");
				if (numbers[0] == 1)
				{
					app->button1Connected = (numbers[1] != 0);
				}
				else if (numbers[0] == 2)
				{
					app->button2Connected = (numbers[1] != 0);
				}
			}
			else
			{
				DEBUG_PrintLine("Only %u numbers in Plug command! \"%s\"", numNumbers, lineStr);
			}
		}
		else
		{
			DEBUG_PrintLine("Got command \"%s\"", lineStr);
		}
		
		switch (app->appState)
		{
			case AppState_Game: ComMessageGameState(lineStr); break;
			case AppState_Setup: ComMessageSetupState(lineStr); break;
		};
	}
	else
	{
		// DEBUG_PrintLine(" => %s", lineStr);
		// DEBUG_Print("[%u]:", lineLength);
		// for (u32 cIndex = 0; cIndex < lineLength; cIndex++)
		// {
		// 	DEBUG_Print(" %02X", lineStr[cIndex]);
		// }
		// DEBUG_WriteLine("");
	}
}

void AppUpdateComConnect()
{
	if (app->comPort.isOpen)
	{
		// +==============================+
		// |         Ping Timeout         |
		// +==============================+
		if (!app->gotPortPing && TimeSince(app->comPortConnectTime) >= PING_WAIT_TIME)
		{
			DEBUG_WriteLine("Timed out! No ping received!");
			platform->CloseComPort(mainHeap, &app->comPort);
			app->button1Connected = false;
			app->button1Down = false;
			app->button2Connected = false;
			app->button2Down = false;
			app->connectPortIndex++;
		}
		else
		{
			// +==============================+
			// |        Read Port Data        |
			// +==============================+
			u32 numDroppedChars = 0;
			i32 readResult = 0;
			do
			{
				u8 tempBuffer[32];
				u32 spaceLeft = MAX_RX_LINE_LENGTH - app->rxLineLength;
				if (spaceLeft > 0)
				{
					readResult = platform->ReadComPort(&app->comPort, &app->rxLineBuffer[app->rxLineLength], spaceLeft);
				}
				else
				{
					readResult = platform->ReadComPort(&app->comPort, tempBuffer, ArrayCount(tempBuffer)-1);
				}
				
				if (readResult > 0)
				{
					if (spaceLeft > 0)
					{
						Assert((u32)readResult <= spaceLeft);
						app->rxLineLength += readResult;
						app->rxLineBuffer[app->rxLineLength] = '\0';
						// DEBUG_PrintLine("Read %d bytes: \"%s\"", readResult, &app->rxLineBuffer[app->rxLineLength-(u32)readResult]);
					}
					else
					{
						Assert(app->rxLineLength == MAX_RX_LINE_LENGTH);
						tempBuffer[readResult] = '\0';
						// DEBUG_PrintLine("Read %d bytes: \"%s\"", readResult, tempBuffer);
						for (i32 bIndex = 0; bIndex < readResult; bIndex++)
						{
							if (tempBuffer[bIndex] == '\n')
							{
								app->rxLineBuffer[MAX_RX_LINE_LENGTH-1] = '\n';
							}
							else
							{
								numDroppedChars++;
							}
						}
					}
				}
				else if (readResult < 0)
				{
					DEBUG_PrintLine("Com Port Disconnected! %d", readResult);
					platform->CloseComPort(mainHeap, &app->comPort);
					app->button1Connected = false;
					app->button1Down = false;
					app->button2Connected = false;
					app->button2Down = false;
					if (app->gotPortPing) { app->connectPortIndex = 0; }
					else { app->connectPortIndex++; }
				}
			} while (readResult > 0);
			
			// +==============================+
			// |        Sanatize Data         |
			// +==============================+
			for (i32 cIndex = 0; (u32)cIndex < app->rxLineLength; cIndex++)
			{
				if (app->rxLineBuffer[cIndex] != '\n' && !IsCharClassPrintable(app->rxLineBuffer[cIndex]))
				{
					if (app->rxLineBuffer[cIndex] == 0x00 || app->rxLineBuffer[cIndex] > 0x05) { DEBUG_PrintLine("Removing unprintable[%u] %02X", cIndex, app->rxLineBuffer[cIndex]); }
					for (u32 cIndex2 = cIndex; cIndex2+1 < app->rxLineLength; cIndex2++)
					{
						app->rxLineBuffer[cIndex2] = app->rxLineBuffer[cIndex2+1];
					}
					app->rxLineLength--;
					cIndex--;
				}
			}
			
			// +==============================+
			// |       Process Rx Lines       |
			// +==============================+
			Assert(app->rxLineLength <= MAX_RX_LINE_LENGTH);
			bool foundLine = false;
			do
			{
				foundLine = false;
				for (u32 lineLength = 0; lineLength < app->rxLineLength; lineLength++)
				{
					if (app->rxLineBuffer[lineLength] == '\n')
					{
						// DEBUG_PrintLine("Processing %u byte line: %s", lineLength, &app->rxLineBuffer[0]);
						app->rxLineBuffer[lineLength] = '\0';
						AppProcessLine(&app->rxLineBuffer[0]);
						for (u32 bIndex = 0; bIndex+lineLength+1 < app->rxLineLength; bIndex++)
						{
							app->rxLineBuffer[bIndex] = app->rxLineBuffer[bIndex + lineLength+1];
						}
						app->rxLineLength -= lineLength+1;
						app->rxLineBuffer[app->rxLineLength] = '\0';
						foundLine = true;
						break;
					}
				}
			} while(foundLine);
		}
	}
	else
	{
		// +==============================+
		// |    Refresh Com Port List     |
		// +==============================+
		if (app->connectPortIndex >= app->portList.count && (app->lastPortsRefreshTime == 0 || TimeSince(app->lastPortsRefreshTime) >= REFRESH_PORT_TIME))
		{
			RefreshComPortList();
			app->connectPortIndex = 0;
			app->lastPortsRefreshTime = platform->programTime;
		}
		
		// +==============================+
		// |       Connect to Port        |
		// +==============================+
		if (app->connectPortIndex < app->portList.count)
		{
			ComSettings_t comSettings = {};
			comSettings.baudRate = BaudRate_115200;
			comSettings.numBits = 8;
			comSettings.parity = Parity_None;
			comSettings.stopBits = StopBits_1;
			DEBUG_PrintLine("Opening port \"%s\"...", app->portList[app->connectPortIndex]);
			app->comPort = platform->OpenComPort(mainHeap, app->portList[app->connectPortIndex], comSettings);
			if (app->comPort.isOpen)
			{
				DEBUG_WriteLine("Connected...");
				app->gotPortPing = false;
				app->lastPortPing = 0;
				app->comPortConnectTime = platform->programTime;
				app->rxLineLength = 0;
				app->rxLineBuffer[0] = '\0';
			}
			else
			{
				DEBUG_WriteLine("Couldn't open port!");
				app->connectPortIndex++;
			}
		}
	}
}

// +--------------------------------------------------------------+
// |                       App Get Version                        |
// +--------------------------------------------------------------+
// Version_t App_GetVersion(bool* resetApplication)
EXPORT AppGetVersion_DEFINITION(App_GetVersion)
{
	Version_t version = { APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD };
	if (resetApplication != nullptr) { *resetApplication = RESET_APPLICATION; }
	return version;
}

// +--------------------------------------------------------------+
// |                        App Initialize                        |
// +--------------------------------------------------------------+
// void App_Initialize(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppInitialize_DEFINITION(App_Initialize)
{
	platform = PlatformInfo;
	input = nullptr;
	appOutput = nullptr;
	app = (AppData_t*)AppMemory->permanantPntr;
	mainHeap = &app->mainHeap;
	game = &app->gameData;
	setup = &app->setupData;
	TempArena = &app->tempArena;
	renderContext = &app->renderContext;
	RenderScreenSize = NewVec2(PlatformInfo->screenSize);
	RenderMousePos = Vec2_Zero;
	RenderMouseStartLeft = Vec2_Zero;
	RenderMouseStartRight = Vec2_Zero;
	
	DEBUG_WriteLine("Initializing application...");
	
	// +==============================+
	// |     Setup Memory Arenas      |
	// +==============================+
	{
		Assert(sizeof(AppData_t) < AppMemory->permanantSize);
		ClearPointer(app);
		
		u32 mainHeapSize = AppMemory->permanantSize - sizeof(AppData_t);
		InitializeMemoryArenaHeap(&app->mainHeap, (void*)(app + 1), mainHeapSize);
		
		InitializeMemoryArenaTemp(&app->tempArena, AppMemory->transientPntr, AppMemory->transientSize, TRANSIENT_MAX_NUMBER_MARKS);
		
		InitializeMemoryArenaStdHeap(&app->stdArena);
		
		DEBUG_PrintLine("Main Heap:  %u bytes", mainHeapSize);
		DEBUG_PrintLine("Temp Arena: %u bytes", AppMemory->transientSize);
	}
	
	TempPushMark();
	 
	// +==============================+
	// |      Initialize Things       |
	// +==============================+
	InitializeRenderContext();
	AppLoadContent(true);
	
	// +==============================+
	// | Initialize Starting AppState |
	// +==============================+
	app->appState = AppState_Setup;
	app->newAppState = app->appState;
	DEBUG_PrintLine("[Initializing AppState_%s]", GetAppStateStr(app->appState));
	switch (app->appState)
	{
		case AppState_Game: InitializeGameState(); StartGameState(app->appState); break;
		case AppState_Setup: InitializeSetupState(); StartSetupState(app->appState); break;
	}
	
	app->soundsVolume = 1.0f;
	app->musicVolume = 1.0f;
	
	DEBUG_WriteLine("Application initialization finished!");
	TempPopMark();
	app->appInitTempHighWaterMark = ArenaGetHighWaterMark(TempArena);
	ArenaResetHighWaterMark(TempArena);
}

// +--------------------------------------------------------------+
// |                          App Update                          |
// +--------------------------------------------------------------+
// void App_Update(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
EXPORT AppUpdate_DEFINITION(App_Update)
{
	StartTimeBlock("AppUpdate");
	
	platform = PlatformInfo;
	input = AppInput;
	appOutput = AppOutput;
	app = (AppData_t*)AppMemory->permanantPntr;
	mainHeap = &app->mainHeap;
	game = &app->gameData;
	setup = &app->setupData;
	TempArena = &app->tempArena;
	renderContext = &app->renderContext;
	RenderScreenSize = NewVec2(PlatformInfo->screenSize);
	RenderMousePos = AppInput->mousePos;
	RenderMouseStartLeft = AppInput->mouseStartPos[MouseButton_Left];
	RenderMouseStartRight = AppInput->mouseStartPos[MouseButton_Right];
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++) { app->buttonHandled[bIndex] = false; }
	
	TempPushMark();
	
	// +==============================+
	// |   Relaod Content Shortcut    |
	// +==============================+
	if (ButtonPressed(Button_F5))
	{
		DEBUG_WriteLine("Reloading app content");
		AppLoadContent(false);
	}
	#if DEBUG
	if (ButtonPressed(Button_F11))
	{
		app->showDebugMenu = !app->showDebugMenu;
	}
	#endif
	
	AppUpdateComConnect();
	UpdateSounds();
	
	// +==============================+
	// |   Update Current AppState    |
	// +==============================+
	switch (app->appState)
	{
		case AppState_Game: UpdateAndRenderGameState(); break;
		case AppState_Setup: UpdateAndRenderSetupState(); break;
	}
	
	// +==============================+
	// |       Change AppStates       |
	// +==============================+
	if (app->newAppState != app->appState)
	{
		//Deinitialize
		if (!app->skipDeinitialization)
		{
			DEBUG_PrintLine("[Deinitializing AppState_%s]", GetAppStateStr(app->appState));
			switch (app->appState)
			{
				case AppState_Game: DeinitializeGameState(); break;
				case AppState_Setup: DeinitializeSetupState(); break;
			}
		}
		
		AppState_t oldState = app->appState;
		app->appState = app->newAppState;
		
		//Initialize
		if (!app->skipInitialization)
		{
			DEBUG_PrintLine("[Initializing AppState_%s]", GetAppStateStr(app->appState));
			switch (app->appState)
			{
				case AppState_Game: InitializeGameState(); break;
				case AppState_Setup: InitializeSetupState(); break;
			}
		}
		
		app->skipInitialization = false;
		app->skipDeinitialization = false;
		
		//Start
		DEBUG_PrintLine("[Starting AppState_%s]", GetAppStateStr(app->appState));
		switch (app->appState)
		{
			case AppState_Game: StartGameState(oldState); break;
			case AppState_Setup: StartSetupState(oldState); break;
		}
	}
	
	// +--------------------------------------------------------------+
	// |                     Render Debug Overlay                     |
	// +--------------------------------------------------------------+
	#if DEBUG
	if (app->showDebugMenu)
	{
		StartTimeBlock("Debug Overlay");
		// +==============================+
		// |      Render Time Blocks      |
		// +==============================+
		TimedBlockInfo_t* appUpdateBlock = GetTimedBlockInfoByName("AppUpdate");
		if (appUpdateBlock != nullptr)
		{
			rec appUpdateRec = NewRec(10, 10, 20, RenderScreenSize.height-20);
			
			RcDrawButton(RecInflate(appUpdateRec, 1), NewColor(Color_LightGrey), NewColor(Color_White));
			
			r32 blockOffset = 0;
			r32 keyOffset = 0;
			u32 bIndex = 0;
			RcBindNewFont(&app->debugFont);
			RcSetFontSize(16);
			RcSetFontAlignment(Alignment_Left);
			RcSetFontStyle(FontStyle_Default);
			
			while (TimedBlockInfo_t* blockInfo = GetTimedBlockInfoByParent("AppUpdate", bIndex))
			{
				r32 percent = (r32)blockInfo->counterElapsed / (r32)appUpdateBlock->counterElapsed;
				rec partRec = appUpdateRec;
				partRec.y += blockOffset;
				partRec.height *= percent;
				Color_t partColor = NewColor(GetColorByIndex(bIndex));
				RcDrawRectangle(partRec, partColor);
				// if (partRec.height >= RcGetLineHeight() || IsInsideRec(partRec, RenderMousePos))
				{
					rec keyColorRec = NewRec(appUpdateRec.topLeft + appUpdateRec.size, NewVec2(10));
					keyColorRec.x += 5;
					keyColorRec.y -= keyColorRec.height + 20 + keyOffset;
					v2 textPos = keyColorRec.topLeft + NewVec2(keyColorRec.width + 2, keyColorRec.height/2 - RcGetLineHeight()/2 + RcGetMaxExtendUp());
					RcDrawButton(keyColorRec, partColor, NewColor(Color_White));
					if (blockInfo->numCalls > 1)
					{
						RcNewPrintString(textPos, NewColor(Color_White), "%s x %u: %.1f%% %lu avg", blockInfo->blockName, blockInfo->numCalls, percent*100, blockInfo->counterElapsed / blockInfo->numCalls);
					}
					else
					{
						RcNewPrintString(textPos, NewColor(Color_White), "%s: %.1f%% %lu", blockInfo->blockName, percent*100, blockInfo->counterElapsed);
					}
					keyOffset += RcGetLineHeight();
				}
				blockOffset += partRec.height;
				bIndex++;
			}
			
			v2 lastTextPos = appUpdateRec.topLeft + appUpdateRec.size;
			lastTextPos.x += 5;
			lastTextPos.y -= 10 + 20 + keyOffset;
			RcNewPrintString(lastTextPos, NewColor(Color_White), "Total: %lu", appUpdateBlock->counterElapsed);
		}
		EndTimeBlock();
	}
	#endif
	
	TempPopMark();
	
	if (ButtonDown(Button_Insert))
	{
		StartTimeBlock("Frame Flip");
		platform->FrameFlip();
		EndTimeBlock();
		
		EndTimeBlock();
	}
	else
	{
		EndTimeBlock();
		
		platform->FrameFlip();
	}
	
	static u8 countdown = 1;
	if (--countdown == 0) { countdown = 10; SaveTimeBlocks(); }
	else { ClearTimeBlocks(); }
}

// +--------------------------------------------------------------+
// |                        App Reloading                         |
// +--------------------------------------------------------------+
// void App_Reloading(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloading_DEFINITION(App_Reloading)
{
	platform = PlatformInfo;
	input = nullptr;
	appOutput = nullptr;
	app = (AppData_t*)AppMemory->permanantPntr;
	mainHeap = &app->mainHeap;
	game = &app->gameData;
	setup = &app->setupData;
	TempArena = &app->tempArena;
	renderContext = &app->renderContext;
	RenderScreenSize = NewVec2(PlatformInfo->screenSize);
	RenderMousePos = Vec2_Zero;
	RenderMouseStartLeft = Vec2_Zero;
	RenderMouseStartRight = Vec2_Zero;
	
	app->lastNumTimedBlockInfos = 0;
}

// +--------------------------------------------------------------+
// |                         App Reloaded                         |
// +--------------------------------------------------------------+
// void App_Reloaded(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloaded_DEFINITION(App_Reloaded)
{
	platform = PlatformInfo;
	input = nullptr;
	appOutput = nullptr;
	app = (AppData_t*)AppMemory->permanantPntr;
	mainHeap = &app->mainHeap;
	game = &app->gameData;
	setup = &app->setupData;
	TempArena = &app->tempArena;
	renderContext = &app->renderContext;
	RenderScreenSize = NewVec2(PlatformInfo->screenSize);
	RenderMousePos = Vec2_Zero;
	RenderMouseStartLeft = Vec2_Zero;
	RenderMouseStartRight = Vec2_Zero;
	
	// TODO: Reallocate stuff that was deallocated in App_Reloading
}

// +--------------------------------------------------------------+
// |                         App Closing                          |
// +--------------------------------------------------------------+
// void App_Closing(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppClosing_DEFINITION(App_Closing)
{
	platform = PlatformInfo;
	input = nullptr;
	appOutput = nullptr;
	app = (AppData_t*)AppMemory->permanantPntr;
	mainHeap = &app->mainHeap;
	game = &app->gameData;
	setup = &app->setupData;
	TempArena = &app->tempArena;
	renderContext = &app->renderContext;
	RenderScreenSize = NewVec2(PlatformInfo->screenSize);
	RenderMousePos = Vec2_Zero;
	RenderMouseStartLeft = Vec2_Zero;
	RenderMouseStartRight = Vec2_Zero;
	
	//TODO: Deallocate anything?
}

#if DEBUG
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	DEBUG_PrintLine("Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, GetFileNamePart(filename), lineNumber, expressionStr);
}
#endif
