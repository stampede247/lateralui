/*
File:   app_wav.cpp
Author: Taylor Robbins
Date:   07\02\2018
Description: 
	** Handles parsing .wav files and loading the sound data into memory
*/

#pragma pack(push, 1)
struct WAV_Header_t
{
	u32 RIFFID;
	u32 size;
	u32 WAVEID;
};

#define RIFF_CODE(a, b, c, d) (((u32)(a) << 0) | ((u32)(b) << 8) | ((u32)(c) << 16) | ((u32)(d) << 24))

enum
{
	WAV_ChunkID_RIFF = RIFF_CODE('R', 'I', 'F', 'F'),
	WAV_ChunkID_WAVE = RIFF_CODE('W', 'A', 'V', 'E'),
	WAV_ChunkID_fmt =  RIFF_CODE('f', 'm', 't', ' '),
	WAV_ChunkID_data = RIFF_CODE('d', 'a', 't', 'a'),
};

struct WAV_Chunk_t
{
	u32 id;
	u32 size;
};

struct WAV_FormatChunk_t
{
	u16 formatTag;
	u16 numChannels;
	u32 numSamplesPerSecond;
	u32 avgBytesPerSec;
	u16 nBlockAlign;
	u16 bitsPerSample;
	u16 cbSize;
	u16 validBitsPerSample;
	u32 dwChannelMask;
	u8 subFormat[16];
};
#pragma pack(pop)

struct RiffIterator_t
{
	u8* pntr;
	u8* stop;
};

inline RiffIterator_t ParseChunkAt(void* dataPntr, void* stop)
{
	RiffIterator_t result;
	result.pntr = (u8*)dataPntr;
	result.stop = (u8*)stop; 
	
	return result;
}

inline RiffIterator_t NextChunk(RiffIterator_t iter)
{
	WAV_Chunk_t* chunk = (WAV_Chunk_t*)iter.pntr;
	
	u32 stepSize = chunk->size;
	if ((stepSize%2) == 1) stepSize++;
	
	iter.pntr += sizeof(WAV_Chunk_t) + stepSize;
	
	return iter;
}

inline bool32 IsValid(RiffIterator_t iter)
{
	bool32 result = (iter.pntr < iter.stop);
	return result;
}

inline void* GetChunkData(RiffIterator_t iter)
{
	void* result = (iter.pntr + sizeof(WAV_Chunk_t));
	
	return result;
}

inline u32 GetType(RiffIterator_t iter)
{
	WAV_Chunk_t* chunk = (WAV_Chunk_t*)iter.pntr;
	u32 result = chunk->id;
	
	return result;
}

inline u32 GetChunkDataSize(RiffIterator_t iter)
{
	WAV_Chunk_t* chunk = (WAV_Chunk_t*)iter.pntr;
	u32 result = chunk->size;
	
	return result;
}


