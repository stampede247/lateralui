/*
File:   game_tray.cpp
Author: Taylor Robbins
Date:   06\17\2018
Description: 
	** Holds the functions that determine the structure and UI of the tray at the bottom of the screen 
*/

void UpdateTray()
{
	r32 scoreLineHeight    = (r32)FontGetLineHeight(&app->scoreFont);
	r32 scoreMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->scoreFont);
	r32 scoreMaxExtendDown = (r32)FontGetMaxExtendDown(&app->scoreFont);
	
	r32 questionLineHeight    = (r32)FontGetLineHeight(&app->questionFont);
	r32 questionMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->questionFont);
	r32 questionMaxExtendDown = (r32)FontGetMaxExtendDown(&app->questionFont);
	
	r32 uiLineHeight    = (r32)FontGetLineHeight(&app->uiFont);
	r32 uiMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->uiFont);
	r32 uiMaxExtendDown = (r32)FontGetMaxExtendDown(&app->uiFont);
	
	r32 clueLineHeight    = (r32)FontGetLineHeight(&app->clueFont);
	r32 clueMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->clueFont);
	r32 clueMaxExtendDown = (r32)FontGetMaxExtendDown(&app->clueFont);
	
	r32 clueNumberLineHeight    = (r32)FontGetLineHeight(&app->clueNumberFont);
	r32 clueNumberMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->clueNumberFont);
	r32 clueNumberMaxExtendDown = (r32)FontGetMaxExtendDown(&app->clueNumberFont);
	
	game->trayRec = NewRec(0, 0, RenderScreenSize.width * 3/4, scoreLineHeight + SCORE_PADDING*2 + BORDER_WIDTH*2);
	game->trayRec.x = RenderScreenSize.width/2 - game->trayRec.width/2;
	game->trayRec.y = RenderScreenSize.height - game->trayRec.height;
	
	game->pointStackLeftRec = NewRec(game->trayRec.topLeft + NewVec2(BORDER_WIDTH), Vec2_Zero);
	game->pointStackLeftRec.height = game->trayRec.height - BORDER_WIDTH*2;
	game->pointStackLeftRec.width = game->pointStackLeftRec.height / 5;
	
	game->pointLeftBaseRec = game->pointStackLeftRec;
	game->pointLeftBaseRec.height = game->pointStackLeftRec.height/5;
	game->pointLeftBaseRec.y = game->pointStackLeftRec.y + game->pointStackLeftRec.height - game->pointLeftBaseRec.height;
	
	game->pointStackRightRec = game->pointStackLeftRec;
	game->pointStackRightRec.x = game->trayRec.x + game->trayRec.width - BORDER_WIDTH - game->pointStackRightRec.width;
	
	game->pointRightBaseRec = game->pointStackRightRec;
	game->pointRightBaseRec.height = game->pointStackRightRec.height/5;
	game->pointRightBaseRec.y = game->pointStackRightRec.y + game->pointStackRightRec.height - game->pointRightBaseRec.height;
	
	game->scoreLeftRec = game->pointStackLeftRec;
	game->scoreLeftRec.width = game->scoreLeftRec.height;
	game->scoreLeftRec.x = game->pointStackLeftRec.x + game->pointStackLeftRec.width + BORDER_WIDTH;
	
	game->scoreLeftTextPos1 = game->scoreLeftRec.topLeft + game->scoreLeftRec.size/2;
	game->scoreLeftTextPos1.y -= scoreLineHeight/2;
	game->scoreLeftTextPos1.y += scoreMaxExtendUp;
	game->scoreLeftTextPos2 = game->scoreLeftTextPos1;
	game->scoreLeftTextPos1.y -= game->scoreLeftRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount);
	game->scoreLeftTextPos2.y += game->scoreLeftRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount);
	char* scoreLeftStr = TempPrint("%u", game->scoreLeft);
	FontMeasureNtString(scoreLeftStr, &app->scoreFont, 0, FontStyle_Default, 0, &game->scoreLeftFlow);
	
	game->scoreRightRec = game->pointStackLeftRec;
	game->scoreRightRec.width = game->scoreRightRec.height;
	game->scoreRightRec.x = game->pointStackRightRec.x - BORDER_WIDTH - game->scoreRightRec.width;
	
	game->scoreRightTextPos1 = game->scoreRightRec.topLeft + game->scoreRightRec.size/2;
	game->scoreRightTextPos1.y -= scoreLineHeight/2;
	game->scoreRightTextPos1.y += scoreMaxExtendUp;
	game->scoreRightTextPos2 = game->scoreRightTextPos1;
	game->scoreRightTextPos1.y -= game->scoreRightRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount);
	game->scoreRightTextPos2.y += game->scoreRightRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount);
	char* scoreRightStr = TempPrint("%u", game->scoreRight);
	FontMeasureNtString(scoreRightStr, &app->scoreFont, 0, FontStyle_Default, 0, &game->scoreRightFlow);
	
	game->questionRec = game->scoreLeftRec;
	game->questionRec.x = game->scoreLeftRec.x + game->scoreLeftRec.width + BORDER_WIDTH;
	game->questionRec.width = game->scoreRightRec.x - BORDER_WIDTH - game->questionRec.x;
	
	ClearStruct(game->questionFlow);
	r32 questionMaxWidth = game->questionRec.width - QUESTION_PADDING*2;
	if (game->questionStr != nullptr)
	{
		FontMeasureNtString(game->questionStr, &app->questionFont, 0, FontStyle_Default, questionMaxWidth, &game->questionFlow);
	}
	game->questionTextPos = game->questionRec.topLeft;
	game->questionTextPos.x += game->questionRec.width / 2;
	game->questionTextPos.y += game->questionRec.height*3/8 - game->questionFlow.totalSize.height/2 + questionMaxExtendUp;
	
	game->lateralLogoPos = game->questionRec.topLeft;
	game->lateralLogoPos.x += game->questionRec.width / 2;
	game->lateralLogoPos.y += game->questionRec.height/2 - scoreLineHeight/2 + scoreMaxExtendUp;
	
	// +==============================+
	// |      Four Point Slider       |
	// +==============================+
	game->fourPointRec.width = game->scoreRightRec.x - (game->scoreLeftRec.x + game->scoreLeftRec.width);
	game->fourPointRec.height = uiLineHeight + FOUR_POINT_PADDING*2;
	game->fourPointRec.x = game->trayRec.x + game->trayRec.width/2 - game->fourPointRec.width/2;
	game->fourPointRec.y = game->trayRec.y - (game->fourPointRec.height + FOUR_POINT_MARGIN)*Ease(game->roundNum == 1 ? EasingStyle_CubicOut : EasingStyle_CubicIn, game->fourPointAnimAmount);
	
	game->fourPointSliderAmount = 1 - ((r32)game->fourPointTimeElapsed / QUESTION_TIME);
	game->fourPointSliderRec = game->fourPointRec;
	game->fourPointSliderRec.width = FOUR_POINT_SLIDER_WIDTH;
	game->fourPointSliderRec.x = game->fourPointRec.x + (game->fourPointRec.width - game->fourPointSliderRec.width) * game->fourPointSliderAmount;
	
	game->fourPointPieceRecs[0] = game->fourPointRec;
	game->fourPointPieceRecs[0].width /= 4;
	game->fourPointPieceRecs[1] = game->fourPointPieceRecs[0] + NewVec2(game->fourPointPieceRecs[0].width, 0);
	game->fourPointPieceRecs[2] = game->fourPointPieceRecs[1] + NewVec2(game->fourPointPieceRecs[1].width, 0);
	game->fourPointPieceRecs[3] = game->fourPointPieceRecs[2] + NewVec2(game->fourPointPieceRecs[2].width, 0);
	
	game->fourPointTextPos[0].x = game->fourPointPieceRecs[0].x + game->fourPointPieceRecs[0].width/2;
	game->fourPointTextPos[0].y = game->fourPointPieceRecs[0].y + game->fourPointPieceRecs[0].height/2 - uiLineHeight/2 + uiMaxExtendUp;
	game->fourPointTextPos[1].x = game->fourPointPieceRecs[1].x + game->fourPointPieceRecs[1].width/2;
	game->fourPointTextPos[1].y = game->fourPointPieceRecs[1].y + game->fourPointPieceRecs[1].height/2 - uiLineHeight/2 + uiMaxExtendUp;
	game->fourPointTextPos[2].x = game->fourPointPieceRecs[2].x + game->fourPointPieceRecs[2].width/2;
	game->fourPointTextPos[2].y = game->fourPointPieceRecs[2].y + game->fourPointPieceRecs[2].height/2 - uiLineHeight/2 + uiMaxExtendUp;
	game->fourPointTextPos[3].x = game->fourPointPieceRecs[3].x + game->fourPointPieceRecs[3].width/2;
	game->fourPointTextPos[3].y = game->fourPointPieceRecs[3].y + game->fourPointPieceRecs[3].height/2 - uiLineHeight/2 + uiMaxExtendUp;
	
	// +==============================+
	// |          Clue Board          |
	// +==============================+
	game->clueBoardRec.height = game->fourPointRec.y - CLUE_BOARD_MARGIN*2;
	game->clueBoardRec.width = game->clueBoardRec.height * (CLUE_BOARD_ASPECT_RATIO);
	game->clueBoardRec.x = RenderScreenSize.width/2 - game->clueBoardRec.width/2;
	game->clueBoardRec.y = game->trayRec.y/2 - game->clueBoardRec.height/2;
	bool showClueBoard = (game->roundNum == 2 && game->showBoard);
	game->clueBoardRec.x += (RenderScreenSize.width - game->clueBoardRec.x) * (1.0f - Ease(showClueBoard ? EasingStyle_BackOut : EasingStyle_CubicOut, game->clueBoardAnimAmount));
	
	rec clueBaseRec = game->clueBoardRec;
	clueBaseRec.size = Vec2Divide(clueBaseRec.size - NewVec2(CLUE_BORDER), NewVec2(4, 3));
	clueBaseRec.size = clueBaseRec.size - NewVec2(CLUE_BORDER);
	clueBaseRec.size = Vec2Round(clueBaseRec.size);
	clueBaseRec.topLeft = game->clueBoardRec.topLeft + NewVec2(CLUE_BORDER);
	for (i32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++) 
	{
		v2i cell = NewVec2i(cIndex%4, cIndex/4);
		game->clueRecs[cIndex] = clueBaseRec;
		game->clueRecs[cIndex].x += (clueBaseRec.width + CLUE_BORDER) * (r32)cell.x;
		game->clueRecs[cIndex].y += (clueBaseRec.height + CLUE_BORDER) * (r32)cell.y;
		
		game->clueTextPos[cIndex] = game->clueRecs[cIndex].topLeft + game->clueRecs[cIndex].size/2;
		const char* clueStr = game->r2Clues[(game->questionNum*R2_NUM_CLUES_PER_Q) + cIndex];
		if (game->roundNum == 2 && clueStr != nullptr && game->clueAnimAmounts[cIndex] > 0.0f)
		{
			FontMeasureNtString(clueStr, &app->clueFont, 0, FontStyle_Default, game->clueRecs[cIndex].width - CLUE_MARGIN, &game->clueFlows[cIndex]);
			game->clueTextPos[cIndex].y += -game->clueFlows[cIndex].totalSize.height/2 + clueMaxExtendUp;
		}
		
		game->clueCoverRecs[cIndex] = game->clueRecs[cIndex];
		r32 openAmount = Ease(game->clueRevealed[cIndex] ? EasingStyle_BounceOut : EasingStyle_Linear, game->clueAnimAmounts[cIndex]);
		game->clueCoverRecs[cIndex].y += game->clueCoverRecs[cIndex].height * openAmount;
		
		game->clueNumberTextPos[cIndex] = game->clueCoverRecs[cIndex].topLeft + game->clueCoverRecs[cIndex].size/2;
		game->clueNumberTextPos[cIndex].y += -clueNumberLineHeight/2 + clueNumberMaxExtendUp;
	}
}

