/*
File:   app_data.h
Author: Taylor Robbins
Date:   03\02\2018
*/

#ifndef _APP_DATA_H
#define _APP_DATA_H

struct TimedBlockInfo_t
{
	const char* blockName;
	const char* parentName;
	u64 counterElapsed;
	u32 numCalls;
	u32 numParents;
};

struct AppData_t
{
	MemoryArena_t mainHeap;
	MemoryArena_t tempArena;
	MemoryArena_t stdArena;
	u32 appInitTempHighWaterMark;
	
	bool buttonHandled[Buttons_NumButtons];
	
	#if DEBUG 
	bool showDebugMenu;
	u32 debugNumTimeBlocks;
	u64 debugTimeBlocks[MAX_TIME_BLOCK_DEPTH];
	const char* debugTimeBlockNames[MAX_TIME_BLOCK_DEPTH];
	
	u32 activeNumTimedBlockInfos;
	TimedBlockInfo_t activeTimedBlockInfos[MAX_TIMED_BLOCKS];
	
	u32 lastNumTimedBlockInfos;
	TimedBlockInfo_t lastTimedBlockInfos[MAX_TIMED_BLOCKS];
	#endif
	
	//AppState Data Structures
	GameData_t gameData;
	SetupData_t setupData;
	
	AppState_t appState;
	AppState_t newAppState;
	bool skipInitialization;
	bool skipDeinitialization;
	
	SoundInstance_t soundInstances[MAX_SOUND_INSTANCES];
	r32 soundsVolume;
	r32 musicVolume;
	SoundTransition_t transitionType;
	u64 musicTransStart;
	u64 musicTransTime;
	SoundInstance_t* currentMusic;
	SoundInstance_t* lastMusic;
	
	RenderContext_t renderContext;
	Shader_t defaultShader;
	Font_t defaultFont;
	NewFont_t scoreFont;
	NewFont_t questionFont;
	NewFont_t uiFont;
	NewFont_t clueFont;
	NewFont_t clueNumberFont;
	NewFont_t debugFont;
	Texture_t testTexture;
	Texture_t turnSprite;
	Sound_t testSound;
	Sound_t round1Sound;
	Sound_t buttonSound;
	Sound_t countdownStartSound;
	Sound_t finalMinuteSound;
	Sound_t thinkingLoop1;
	Sound_t thinkingLoop2;
	Sound_t successSound;
	Sound_t failureSound;
	Sound_t finishSound;
	Sound_t finishSong;
	
	BoundedStrList_t portList;
	u32 connectPortIndex;
	ComPort_t comPort;
	u64 comPortConnectTime;
	u64 lastPortsRefreshTime;
	
	bool gotPortPing;
	u64 lastPortPing;
	bool button1Connected;
	bool button1Down;
	bool button2Connected;
	bool button2Down;
	
	u32 rxLineLength;
	char rxLineBuffer[MAX_RX_LINE_LENGTH+1];
	
	char* questionPackDir;
	char* leftTeamName;
	char* rightTeamName;
};

#endif //  _APP_DATA_H
