/*
File:   setup.cpp
Author: Taylor Robbins
Date:   06\22\2018
Description: 
	** Holds the Setup GameState that handles all the configuration and information entering process 
*/

void GetPackInfo(const char* folderPath, PackInfo_t* info)
{
	Assert(folderPath != nullptr);
	Assert(info != nullptr);
	
	ClearPointer(info);
	if (!platform->DoesFolderExist(folderPath)) { DEBUG_PrintLine("Pack folder \"%s\" doesn't exist!", folderPath); return; }
	
	TempPushMark();
	for (u32 rIndex = 0; rIndex < 3; rIndex++)
	{
		DEBUG_PrintLine("folderPath: %s", folderPath);
		char* roundFilePath = TempPrint("%s/round%u.txt", folderPath, rIndex+1);
		if (platform->DoesFileExist(roundFilePath))
		{
			info->rounds[rIndex].fileExists = true;
			FileInfo_t roundFile = platform->ReadEntireFile(roundFilePath);
			const char* fileData = (const char*)roundFile.content;
			
			u32 lineStartPos = 0;
			for (u32 cIndex = 0; cIndex <= roundFile.size; cIndex++)
			{
				char c = cIndex < roundFile.size ? fileData[cIndex] : '\n';
				if (c == '\n' || c == '\r' || cIndex == roundFile.size)
				{
					u32 lineLength = cIndex - lineStartPos;
					const char* linePntr = &fileData[lineStartPos];
					if (lineLength >= 3 && linePntr[1] == ':')
					{
						if (linePntr[0] == 'Q') { info->rounds[rIndex].numQuestions++; }
						else if (linePntr[0] == 'C') { info->rounds[rIndex].numClues++; }
						else if (linePntr[0] == 'A') { info->rounds[rIndex].numAnswers++; }
						else
						{
							DEBUG_PrintLine("Unknown line type %c: \"%.*s\"", linePntr[0], lineLength, linePntr);
						}
					}
					else if (lineLength > 0)
					{
						DEBUG_PrintLine("Unknown line: \"%.*s\"", lineLength, linePntr);
					}
					lineStartPos = cIndex+1;
				}
			}
		}
		else
		{
			DEBUG_PrintLine("Round file doesn't exist \"%s\"", roundFilePath);
		}
	}
	TempPopMark();
}

void ReloadPacks()
{
	if (setup->packInfos != nullptr)
	{
		ArenaPop(mainHeap, setup->packInfos);
		setup->packInfos = nullptr;
	}
	BoundedStrListDestroy(&setup->packList, mainHeap);
	
	bool foundSelectedDir = false;
	u32 numFolders = platform->GetNumFilesInFolder(QUESTIONS_FOLDER, false, true);
	DEBUG_PrintLine("Found %u folders!", numFolders);
	if (numFolders > 0)
	{
		setup->packInfos = PushArray(mainHeap, PackInfo_t, numFolders);
		BoundedStrListCreate(&setup->packList, numFolders, 32, mainHeap);
		for (u32 fIndex = 0; fIndex < numFolders; fIndex++)
		{
			TempPushMark();
			char* folderName = platform->GetFileInFolder(TempArena, QUESTIONS_FOLDER, fIndex, false, true);
			char* folderPath = StrCat(TempArena, NtStr(QUESTIONS_FOLDER), NtStr(folderName));
			BoundedStrListAdd(&setup->packList, folderPath);
			DEBUG_PrintLine("  [%u]: %s", fIndex, folderPath);
			GetPackInfo(folderPath, &setup->packInfos[fIndex]);
			if (app->questionPackDir != nullptr && strcmp(app->questionPackDir, folderPath) == 0) { foundSelectedDir = true; }
			TempPopMark();
		}
	}
	
	if (app->questionPackDir != nullptr && !foundSelectedDir)
	{
		ArenaPop(mainHeap, app->questionPackDir);
		app->questionPackDir = nullptr;
	}
}

// +--------------------------------------------------------------+
// |                          Initialize                          |
// +--------------------------------------------------------------+
void InitializeSetupState()
{
	//TODO: Allocate stuff?
	
	setup->leftTeamName = NewTextBox(NewRec(10, 300, 300, 20), nullptr, 0, 128, mainHeap, &app->defaultFont);
	setup->rightTeamName = NewTextBox(NewRec(RenderScreenSize.width - 310, 300, 300, 20), nullptr, 0, 128, mainHeap, &app->defaultFont);
	if (app->leftTeamName != nullptr) { TextBoxSet(&setup->leftTeamName, NtStr(app->leftTeamName)); }
	if (app->rightTeamName != nullptr) { TextBoxSet(&setup->rightTeamName, NtStr(app->rightTeamName)); }
	
	setup->initialized = true;
}

// +--------------------------------------------------------------+
// |                            Start                             |
// +--------------------------------------------------------------+
void StartSetupState(AppState_t fromState)
{
	ReloadPacks();
}

// +--------------------------------------------------------------+
// |                         Deinitialize                         |
// +--------------------------------------------------------------+
void DeinitializeSetupState()
{
	//TODO: Deallocate stuff?
	if (setup->packInfos != nullptr)
	{
		ArenaPop(mainHeap, setup->packInfos);
	}
	BoundedStrListDestroy(&setup->packList, mainHeap);
	ArenaPop(mainHeap, setup->leftTeamName.chars);
	ArenaPop(mainHeap, setup->rightTeamName.chars);
	
	ClearPointer(setup);
}

// +--------------------------------------------------------------+
// |                         Com Message                          |
// +--------------------------------------------------------------+
void ComMessageSetupState(const char* messageStr)
{
	//TODO: Process messages
}

// +--------------------------------------------------------------+
// |                      Update And Render                       |
// +--------------------------------------------------------------+
void UpdateAndRenderSetupState()
{
	rec connectStatusRec = NewRec(10, 10, 100, 20);
	const char* connectStatusStr = "Unknown";
	rec button2Rec = NewRec(0, 0, 50, 50);
	button2Rec.topLeft = RenderScreenSize - button2Rec.size - NewVec2(10);
	rec button1Rec = button2Rec;
	button1Rec.x -= button1Rec.width + 10;
	rec resetButton = NewRec(0, 10, 200, 40);
	resetButton.x = RenderScreenSize.width - resetButton.width - 10;
	rec reloadButton = connectStatusRec;
	reloadButton.y += connectStatusRec.height + 5;
	rec packButtonBaseRec = NewRec(0, 100, RenderScreenSize.width/2, 30);
	packButtonBaseRec.x = RenderScreenSize.width/2 - packButtonBaseRec.width/2;
	rec* packButtonRecs = nullptr;
	
	// +--------------------------------------------------------------+
	// |                            Update                            |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// | Update Connect Status Button |
		// +==============================+
		{
			if (app->comPort.isOpen)
			{
				if (app->gotPortPing)
				{
					connectStatusStr = TempPrint("Connected to %s", app->comPort.name);
				}
				else
				{
					connectStatusStr = TempPrint("Connecting to %s...", app->comPort.name);
				}
			}
			else if (app->connectPortIndex >= app->portList.count)
			{
				connectStatusStr = "Can't find Button Controller!";
			}
			else
			{
				connectStatusStr = "Connecting...";
			}
			
			v2 textSize = FontMeasureNtString(connectStatusStr, &app->debugFont, 12, FontStyle_Default);
			if (connectStatusRec.width < textSize.width + 10)
			{
				connectStatusRec.width = textSize.width + 10;
			}
		}
		
		// +==============================+
		// |          Go to Game          |
		// +==============================+
		if (app->comPort.isOpen && app->gotPortPing && app->button1Connected && app->button2Connected && app->questionPackDir != nullptr)
		{
			if (ButtonPressed(Button_Enter))
			{
				AppChangeState(AppState_Game, game->initialized, true);
			}
		}
		
		// +==============================+
		// |      Reset Game Clicked      |
		// +==============================+
		if (game->initialized && ClickedOnRec(resetButton))
		{
			DeinitializeGameState();
		}
		
		// +==============================+
		// |         Reload Packs         |
		// +==============================+
		if (ClickedOnRec(reloadButton))
		{
			ReloadPacks();
		}
		
		// +==============================+
		// |          Pack Recs           |
		// +==============================+
		if (setup->packList.count > 0)
		{
			packButtonRecs = TempArray(rec, setup->packList.count);
			for (u32 pIndex = 0; pIndex < setup->packList.count; pIndex++)
			{
				packButtonRecs[pIndex] = packButtonBaseRec;
				packButtonRecs[pIndex].y += pIndex * (packButtonBaseRec.height + 5);
				
				if (ClickedOnRec(packButtonRecs[pIndex]))
				{
					if (app->questionPackDir != nullptr)
					{
						ArenaPop(mainHeap, app->questionPackDir);
					}
					app->questionPackDir = ArenaNtString(mainHeap, setup->packList[pIndex]);
				}
			}
		}
		
		// +==============================+
		// |       Update TextBoxes       |
		// +==============================+
		if (ButtonPressed(MouseButton_Left))
		{
			setup->leftTeamNameSelected = false;
			setup->rightTeamNameSelected = false;
			
			if (IsInsideRec(setup->leftTeamName.drawRec, RenderMousePos))
			{
				setup->leftTeamNameSelected = true;
			}
			if (IsInsideRec(setup->rightTeamName.drawRec, RenderMousePos))
			{
				setup->rightTeamNameSelected = true;
			}
		}
		if (setup->rightTeamName.drawRec.x + setup->rightTeamName.drawRec.width + 10 != RenderScreenSize.width)
		{
			TextBoxRellocate(&setup->leftTeamName,  NewRec(10, 300, 300, app->defaultFont.lineHeight + 10));
			TextBoxRellocate(&setup->rightTeamName, NewRec(RenderScreenSize.width - 310, 300, 300, app->defaultFont.lineHeight + 10));
		}
		TextBoxUpdate(&setup->leftTeamName, setup->leftTeamNameSelected);
		TextBoxUpdate(&setup->rightTeamName, setup->rightTeamNameSelected);
		if (app->leftTeamName == nullptr || strncmp(app->leftTeamName, setup->leftTeamName.chars, setup->leftTeamName.numChars) != 0)
		{
			if (setup->leftTeamName.numChars > 0)
			{
				if (app->leftTeamName != nullptr) { ArenaPop(mainHeap, app->leftTeamName); }
				app->leftTeamName = ArenaString(mainHeap, setup->leftTeamName.chars, setup->leftTeamName.numChars);
			}
		}
		if (app->rightTeamName == nullptr || strncmp(app->rightTeamName, setup->rightTeamName.chars, setup->rightTeamName.numChars) != 0)
		{
			if (setup->rightTeamName.numChars > 0)
			{
				if (app->rightTeamName != nullptr) { ArenaPop(mainHeap, app->rightTeamName); }
				app->rightTeamName = ArenaString(mainHeap, setup->rightTeamName.chars, setup->rightTeamName.numChars);
			}
		}
	}
	
	// +--------------------------------------------------------------+
	// |                            Render                            |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |            Setup             |
		// +==============================+
		{
			RcBegin(&app->defaultShader, &app->defaultFont, NewRec(Vec2_Zero, RenderScreenSize));
			RcClearColorBuffer(NewColor(Color_MidnightBlue));
			RcClearDepthBuffer(1.0f);
			RcBindNewFont(&app->debugFont);
			RcSetFontSize(12);
			RcSetFontAlignment(Alignment_Center);
			RcSetFontStyle(FontStyle_Default);
		}
		
		r32 lineHeight    = RcGetLineHeight();
		r32 maxExtendUp   = RcGetMaxExtendUp();
		r32 maxExtendDown = RcGetMaxExtendDown();
		
		// +==============================+
		// |         Render Title         |
		// +==============================+
		RcSetFontSize(24);
		RcNewDrawNtString("Configuration", NewVec2(RenderScreenSize.width/2, 10 + maxExtendUp*2), NewColor(Color_White));
		if (app->comPort.isOpen && app->gotPortPing && app->button1Connected && app->button2Connected && app->questionPackDir != nullptr)
		{
			RcNewDrawNtString("(Press Enter to continue)", NewVec2(RenderScreenSize.width/2, 10 + lineHeight*2 + maxExtendUp*2), NewColor(Color_White));
		}
		RcSetFontSize(12);
		
		// +==================================+
		// | Render Connection Status Button  |
		// +==================================+
		{
			Color_t backColor = NewColor(Color_White);
			Color_t textColor = NewColor(Color_Black);
			if (app->comPort.isOpen)
			{
				if (app->gotPortPing)
				{
					backColor = NewColor(Color_Green);
					textColor = NewColor(Color_White);
				}
				else
				{
					backColor = NewColor(Color_Orange);
				}
			}
			RcDrawButton(connectStatusRec, backColor, NewColor(Color_Black), 1);
			
			v2 textPos = connectStatusRec.topLeft + connectStatusRec.size/2 + NewVec2(0, -lineHeight/2 + maxExtendUp);
			RcNewDrawNtString(connectStatusStr, textPos, textColor);
		}
		
		// +==============================+
		// |      Render Button Recs      |
		// +==============================+
		{
			Color_t backColor = NewColor(Color_Gray);
			if (app->button1Connected)
			{
				backColor = NewColor(Color_White);
				if (app->button1Down)
				{
					backColor = NewColor(Color_Red);
				}
			}
			RcDrawButton(button1Rec, backColor, NewColor(Color_Black), 1);
		}
		{
			Color_t backColor = NewColor(Color_Gray);
			if (app->button2Connected)
			{
				backColor = NewColor(Color_White);
				if (app->button2Down)
				{
					backColor = NewColor(Color_Red);
				}
			}
			RcDrawButton(button2Rec, backColor, NewColor(Color_Black), 1);
		}
		
		// +==============================+
		// |     Render Reset Button      |
		// +==============================+
		if (game->initialized)
		{
			Color_t backColor = NewColor(Color_White);
			Color_t textColor = NewColor(Color_Black);
			if (IsInsideRec(resetButton, RenderMousePos))
			{
				backColor = NewColor(Color_LightGrey);
				if (IsInsideRec(resetButton, RenderMouseStartLeft) && ButtonDown(MouseButton_Left))
				{
					backColor = NewColor(Color_Red);
					textColor = NewColor(Color_White);
				}
			}
			RcDrawButton(resetButton, backColor, textColor, 1);
			v2 textPos = resetButton.topLeft + resetButton.size/2 + NewVec2(0, -lineHeight/2 + maxExtendUp);
			RcNewDrawNtString("Reset Game", textPos, textColor);
		}
		
		// +==============================+
		// |      Draw Reload Button      |
		// +==============================+
		{
			Color_t backColor = NewColor(Color_White);
			Color_t textColor = NewColor(Color_Black);
			if (IsInsideRec(reloadButton, RenderMousePos))
			{
				backColor = NewColor(Color_LightGrey);
				if (IsInsideRec(reloadButton, RenderMouseStartLeft) && ButtonDown(MouseButton_Left))
				{
					backColor = NewColor(Color_Red);
					textColor = NewColor(Color_White);
				}
			}
			RcDrawButton(reloadButton, backColor, textColor, 1);
			v2 textPos = reloadButton.topLeft + reloadButton.size/2 + NewVec2(0, -lineHeight/2 + maxExtendUp);
			RcNewDrawNtString("Reload Packs", textPos, textColor);
		}
		
		// +==============================+
		// |      Draw Pack Buttons       |
		// +==============================+
		for (u32 pIndex = 0; pIndex < setup->packList.count; pIndex++)
		{
			bool selected = (app->questionPackDir != nullptr && strcmp(app->questionPackDir, setup->packList[pIndex]) == 0);
			Color_t backColor = NewColor(Color_White);
			Color_t textColor = NewColor(Color_Black);
			Color_t borderColor = NewColor(Color_Black);
			if (selected)
			{
				borderColor = NewColor(Color_White);
				backColor = NewColor(Color_OrangeRed);
			}
			if (IsInsideRec(packButtonRecs[pIndex], RenderMousePos))
			{
				backColor = NewColor(selected ? Color_Orange : Color_LightGrey);
				if (IsInsideRec(packButtonRecs[pIndex], RenderMouseStartLeft) && ButtonDown(MouseButton_Left))
				{
					backColor = NewColor(Color_Red);
					textColor = NewColor(Color_White);
				}
			}
			RcDrawButton(packButtonRecs[pIndex], backColor, borderColor, 1);
			v2 textPos = packButtonRecs[pIndex].topLeft + packButtonRecs[pIndex].size/2 + NewVec2(0, -lineHeight/2 + maxExtendUp);
			
			TempPushMark();
			PackInfo_t* packInfo = &setup->packInfos[pIndex];
			char* packString = setup->packList[pIndex];
			if (packInfo->round1FileExists)
			{
				packString = TempPrint("%s (%s%u\x02.%s%u\x02.%s%u\x02)", packString,
					(packInfo->round1NumQuestions != R1_NUM_QUESTIONS)                    ? "\x01\xFF\x01\x01" : "", packInfo->round1NumQuestions,
					(packInfo->round1NumClues     != R1_NUM_QUESTIONS*R1_NUM_CLUES_PER_Q) ? "\x01\xFF\x01\x01" : "", packInfo->round1NumClues,
					(packInfo->round1NumAnswers   != R1_NUM_QUESTIONS)                    ? "\x01\xFF\x01\x01" : "", packInfo->round1NumAnswers
				);
			}
			else
			{
				packString = TempPrint("%s (\x01\xFF\x01\x01None\x02)", packString);
			}
			if (packInfo->round2FileExists)
			{
				packString = TempPrint("%s(%s%u\x02.%s%u\x02.%s%u\x02)", packString,
					(packInfo->round2NumQuestions != 0)                    ? "\x01\xFF\x01\x01" : "", packInfo->round2NumQuestions,
					(packInfo->round2NumClues     != R2_NUM_QUESTIONS*R2_NUM_CLUES_PER_Q) ? "\x01\xFF\x01\x01" : "", packInfo->round2NumClues,
					(packInfo->round2NumAnswers   != R2_NUM_QUESTIONS)                    ? "\x01\xFF\x01\x01" : "", packInfo->round2NumAnswers
				);
			}
			else
			{
				packString = TempPrint("%s(\x01\xFF\x01\x01None\x02)", packString);
			}
			if (packInfo->round3FileExists)
			{
				packString = TempPrint("%s(%u.%s%u\x02.%s%u\x02)", packString,
					packInfo->round3NumQuestions,
					(packInfo->round3NumClues     != packInfo->round3NumQuestions*R3_NUM_CLUES_PER_Q) ? "\x01\xFF\x01\x01" : "", packInfo->round3NumClues,
					(packInfo->round3NumAnswers   != packInfo->round3NumQuestions)                    ? "\x01\xFF\x01\x01" : "", packInfo->round3NumAnswers
				);
			}
			else
			{
				packString = TempPrint("%s(\x01\xFF\x01\x01None\x02)", packString);
			}
			RcNewDrawNtString(packString, textPos, textColor);
			TempPopMark();
		}
		
		// +==============================+
		// |        Draw Textboxes        |
		// +==============================+
		TextBoxRender(&setup->leftTeamName, setup->leftTeamNameSelected);
		TextBoxRender(&setup->rightTeamName, setup->rightTeamNameSelected);
	}
}
