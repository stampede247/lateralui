/*
File:   setup.h
Author: Taylor Robbins
Date:   06\22\2018
*/

#ifndef _SETUP_H
#define _SETUP_H

union PackInfo_t
{
	struct
	{
		bool fileExists;
		u32 numQuestions;
		u32 numClues;
		u32 numAnswers;
	} rounds[3];
	struct
	{
		bool round1FileExists;
		u32 round1NumQuestions;
		u32 round1NumClues;
		u32 round1NumAnswers;
		
		bool round2FileExists;
		u32 round2NumQuestions;
		u32 round2NumClues;
		u32 round2NumAnswers;
		
		bool round3FileExists;
		u32 round3NumQuestions;
		u32 round3NumClues;
		u32 round3NumAnswers;
	};
};

struct SetupData_t
{
	bool initialized;
	
	BoundedStrList_t packList;
	PackInfo_t* packInfos;
	
	bool leftTeamNameSelected;
	TextBox_t leftTeamName;
	bool rightTeamNameSelected;
	TextBox_t rightTeamName;
};

#endif //  _SETUP_H
