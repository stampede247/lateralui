/*
File:   app_defines.h
Author: Taylor Robbins
Date:   03\02\2018
*/

#ifndef _APP_DEFINES_H
#define _APP_DEFINES_H

#define TRANSIENT_MAX_NUMBER_MARKS 16
#define TAB_WIDTH 4
#define MAX_TIME_BLOCK_DEPTH 16
#define MAX_TIMED_BLOCKS 128

#define FONTS_FOLDER     "Resources/Fonts/"
#define MUSIC_FOLDER     "Resources/Music/"
#define SHADERS_FOLDER   "Resources/Shaders/"
#define SOUNDS_FOLDER    "Resources/Sounds/"
#define SCRIPTS_FOLDER   "Resources/Scripts/"
#define SPRITES_FOLDER   "Resources/Sprites/"
#define TEXTURES_FOLDER  "Resources/Textures/"
#define QUESTIONS_FOLDER "Resources/Questions/"

#define MISSING_SPRITE_PATH  SPRITES_FOLDER "missing.png"
#define MISSING_TEXTURE_PATH TEXTURES_FOLDER "missing.jpg"

#define SCORE_FONT_SIZE       116
#define QUESTION_FONT_SIZE    32
#define UI_FONT_SIZE          28
#define CLUE_FONT_SIZE        36
#define CLUE_NUMBER_FONT_SIZE 96

#define REFRESH_PORT_TIME     5000 //ms
#define PING_WAIT_TIME        3000 //ms
#define MAX_RX_LINE_LENGTH    128 //chars
#define MAX_SOUND_INSTANCES   16

#define SCORE_PADDING             64 //px
#define BORDER_WIDTH              10 //px
#define QUESTION_PADDING          10 //px
#define FOUR_POINT_PADDING        8  //px
#define FOUR_POINT_MARGIN         10 //px
#define FOUR_POINT_SLIDER_WIDTH   12 //px
#define FOUR_POINT_DIVIDER_WIDTH  6  //px
#define CLUE_BOARD_MARGIN         50
#define CLUE_BORDER               15 //px
#define CLUE_MARGIN               5  //px
#define CLUE_BOARD_ASPECT_RATIO   (6.f/3.f)
#define BUTTON_PRESS_FADE_TIME    1000 //ms
#define VOLUMES_SHOW_TIME         1000

#define QUESTION_TIME  (60*1000) //ms (used during phase 1)
#define R1_NUM_QUESTIONS   6
#define R1_NUM_CLUES_PER_Q 3
#define R2_NUM_QUESTIONS   4
#define R2_NUM_CLUES_PER_Q 12
#define R3_NUM_CLUES_PER_Q 3
#define R3_CLUE_TIME       10 //seconds
#define R3_BASE_TIME       90 //seconds
#define R3_TIME_PER_POINT  5 //seconds
#define R3_PENALTY_TIME    10 //seconds

#define TrayColor            NewColor(0xFF555555)
#define TrayColorDark        NewColor(0xFF333333)
#define TrayColorLight       NewColor(0xFF777777)
#define QuestionWindowColor  NewColor(0xFF222222)
#define PanelColor1          NewColor(0xFF2B2B2B)
#define PanelColor2          NewColor(0xFF282828)
#define ScoreColor           NewColor(Color_White)
#define SliderColor1         NewColor(Color_Green)
#define SliderColor2         NewColor(Color_GreenYellow)
#define ClueBackColor1       NewColor(Color_DodgerBlue)
#define ClueBackColor2       ColorLerp(NewColor(Color_DodgerBlue), NewColor(Color_Blue), 0.1f)
#define ClueCoverColor1      NewColor(0xFF444444)
#define ClueCoverColor2      NewColor(0xFF444444)
#define ClueNumberColor      ColorTransparent(NewColor(Color_White), 0.7f)
#define LeftColor            NewColor(0xFFFF9533) //Orange
#define LeftColorDark        NewColor(0xFFB95900) //Dark Orange
#define RightColor           NewColor(0xFF4169E1) //Blue
#define RightColorDark       NewColor(0xFF07257D) //Dark Blue

#endif //  _APP_DEFINES_H
