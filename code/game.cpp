/*
File:   game.cpp
Author: Taylor Robbins
Date:   03\03\2018
Description: 
	** Holds the functions for AppState_Game and includes any other source files needed to run the game
*/

// +--------------------------------------------------------------+
// |                   Public Helper Functions                    |
// +--------------------------------------------------------------+


// +--------------------------------------------------------------+
// |                      Game Source Files                       |
// +--------------------------------------------------------------+
#include "game_tray.cpp"

// +--------------------------------------------------------------+
// |                   Private Helper Functions                   |
// +--------------------------------------------------------------+
void GameLoadContent()
{
	if (game->initialized)
	{
		DestroyTexture(&game->testTexture);
		DestroyTexture(&game->circuitTexture);
		DestroyTexture(&game->missingTexture);
		DestroyTexture(&game->canvasTexture);
	}
	
	game->testTexture    = LoadTexture(TEXTURES_FOLDER "test.png");
	game->circuitTexture = LoadTexture(TEXTURES_FOLDER "circuit.png");
	game->missingTexture = LoadTexture(TEXTURES_FOLDER "something_that_doesnt_exit.png");
	game->canvasTexture  = LoadTexture(TEXTURES_FOLDER "canvas.jpg");
}

void DeallocateQuestions()
{
	for (u32 qIndex = 0; qIndex < R1_NUM_QUESTIONS; qIndex++)
	{
		if (game->r1Questions[qIndex] != nullptr) { ArenaPop(mainHeap, game->r1Questions[qIndex]); game->r1Questions[qIndex] = nullptr; }
		if (game->r1Answers[qIndex] != nullptr) { ArenaPop(mainHeap, game->r1Answers[qIndex]); game->r1Answers[qIndex] = nullptr; }
		for (u32 cIndex = 0; cIndex < R1_NUM_CLUES_PER_Q; cIndex++)
		{
			u32 clueIndex = (qIndex*R1_NUM_CLUES_PER_Q) + cIndex;
			if (game->r1Clues[clueIndex] != nullptr) { ArenaPop(mainHeap, game->r1Clues[clueIndex]); game->r1Clues[clueIndex] = nullptr; }
		}
	}
	for (u32 qIndex = 0; qIndex < R2_NUM_QUESTIONS; qIndex++)
	{
		if (game->r2Answers[qIndex] != nullptr) { ArenaPop(mainHeap, game->r2Answers[qIndex]); game->r2Answers[qIndex] = nullptr; }
		for (u32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++)
		{
			u32 clueIndex = (qIndex*R2_NUM_CLUES_PER_Q) + cIndex;
			if (game->r2Clues[clueIndex] != nullptr) { ArenaPop(mainHeap, game->r2Clues[clueIndex]); game->r2Clues[clueIndex] = nullptr; }
		}
	}
	for (u32 qIndex = 0; qIndex < game->r3NumQuestions; qIndex++)
	{
		if (game->r3Questions != nullptr && game->r3Questions[qIndex] != nullptr) { ArenaPop(mainHeap, game->r3Questions[qIndex]); game->r3Questions[qIndex] = nullptr; }
		if (game->r3Answers != nullptr && game->r3Answers[qIndex] != nullptr) { ArenaPop(mainHeap, game->r3Answers[qIndex]); game->r3Answers[qIndex] = nullptr; }
		if (game->r3Clues != nullptr)
		{
			for (u32 cIndex = 0; cIndex < R3_NUM_CLUES_PER_Q; cIndex++)
			{
				u32 clueIndex = (qIndex*R3_NUM_CLUES_PER_Q) + cIndex;
				if (game->r3Clues[clueIndex] != nullptr) { ArenaPop(mainHeap, game->r3Clues[clueIndex]); game->r3Clues[clueIndex] = nullptr; }
			}
		}
		if (game->r3Questions != nullptr) { ArenaPop(mainHeap, game->r3Questions); game->r3Questions = nullptr; }
		if (game->r3Answers != nullptr) { ArenaPop(mainHeap, game->r3Answers); game->r3Answers = nullptr; }
		if (game->r3Clues != nullptr) { ArenaPop(mainHeap, game->r3Clues); game->r3Clues = nullptr; }
	}
}

void GameLoadQuestions()
{
	DeallocateQuestions();
	
	Assert(app->questionPackDir != nullptr);
	
	//Round 1
	{
		char* filePath = StrCat(TempArena, NtStr(app->questionPackDir), NtStr("/round1.txt"));
		u32 numQuestions = 0;
		u32 numClues = 0;
		u32 numAnswers = 0;
		
		FileInfo_t roundFile = platform->ReadEntireFile(filePath);
		const char* fileData = (const char*)roundFile.content;
		u32 lineStartPos = 0;
		for (u32 cIndex = 0; cIndex <= roundFile.size; cIndex++)
		{
			char c = (cIndex < roundFile.size) ? fileData[cIndex] : '\n';
			if (c == '\n' || c == '\r' || cIndex == roundFile.size)
			{
				u32 lineLength = cIndex - lineStartPos;
				const char* linePntr = &fileData[lineStartPos];
				if (lineLength >= 3 && linePntr[1] == ':')
				{
					if (linePntr[0] == 'Q')
					{
						Assert(numQuestions < R1_NUM_QUESTIONS);
						Assert(game->r1Questions[numQuestions] == nullptr);
						game->r1Questions[numQuestions] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
						numQuestions++;
						numClues = 0;
						numAnswers = 0;
					}
					else if (linePntr[0] == 'C')
					{
						Assert(numQuestions > 0);
						Assert(numClues < R1_NUM_CLUES_PER_Q);
						Assert(game->r1Clues[((numQuestions-1)*R1_NUM_CLUES_PER_Q) + numClues] == nullptr);
						game->r1Clues[((numQuestions-1)*R1_NUM_CLUES_PER_Q) + numClues] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
						numClues++;
					}
					else if (linePntr[0] == 'A')
					{
						Assert(numQuestions > 0 && numQuestions <= R1_NUM_QUESTIONS);
						Assert(numAnswers == 0);
						Assert(game->r1Answers[numQuestions-1] == nullptr);
						game->r1Answers[numQuestions-1] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
						numAnswers++;
					}
					else
					{
						DEBUG_PrintLine("Unknown line type %c: \"%.*s\"", linePntr[0], lineLength, linePntr);
					}
				}
				else if (lineLength > 0)
				{
					DEBUG_PrintLine("Unknown line: \"%.*s\"", lineLength, linePntr);
				}
				lineStartPos = cIndex+1;
			}
		}
		
		DEBUG_PrintLine("Loaded Round1 %u questions", numQuestions);
	}
	
	//Round 2
	{
		char* filePath = StrCat(TempArena, NtStr(app->questionPackDir), NtStr("/round2.txt"));
		u32 numClues = 0;
		u32 numAnswers = 0;
		
		FileInfo_t roundFile = platform->ReadEntireFile(filePath);
		const char* fileData = (const char*)roundFile.content;
		u32 lineStartPos = 0;
		for (u32 cIndex = 0; cIndex <= roundFile.size; cIndex++)
		{
			char c = (cIndex < roundFile.size) ? fileData[cIndex] : '\n';
			if (c == '\n' || c == '\r' || cIndex == roundFile.size)
			{
				u32 lineLength = cIndex - lineStartPos;
				const char* linePntr = &fileData[lineStartPos];
				if (lineLength >= 3 && linePntr[1] == ':')
				{
					if (linePntr[0] == 'A')
					{
						Assert(numAnswers < R2_NUM_QUESTIONS);
						Assert(game->r2Answers[numAnswers] == nullptr);
						game->r2Answers[numAnswers] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
						numAnswers++;
						numClues = 0;
					}
					else if (linePntr[0] == 'C')
					{
						Assert(numAnswers > 0);
						Assert(numClues < R2_NUM_CLUES_PER_Q);
						Assert(game->r2Clues[((numAnswers-1)*R2_NUM_CLUES_PER_Q) + numClues] == nullptr);
						game->r2Clues[((numAnswers-1)*R2_NUM_CLUES_PER_Q) + numClues] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
						numClues++;
					}
					else
					{
						DEBUG_PrintLine("Unknown line type %c: \"%.*s\"", linePntr[0], lineLength, linePntr);
					}
				}
				else if (lineLength > 0)
				{
					DEBUG_PrintLine("Unknown line: \"%.*s\"", lineLength, linePntr);
				}
				lineStartPos = cIndex+1;
			}
		}
		
		DEBUG_PrintLine("Loaded Round2 %u answers", numAnswers);
	}
	
	//Round 3
	{
		char* filePath = StrCat(TempArena, NtStr(app->questionPackDir), NtStr("/round3.txt"));
		u32 numQuestions = 0;
		u32 numClues = 0;
		u32 numAnswers = 0;
		
		FileInfo_t roundFile = platform->ReadEntireFile(filePath);
		const char* fileData = (const char*)roundFile.content;
		
		//Count first
		{
			u32 lineStartPos = 0;
			for (u32 cIndex = 0; cIndex <= roundFile.size; cIndex++)
			{
				char c = (cIndex < roundFile.size) ? fileData[cIndex] : '\n';
				if (c == '\n' || c == '\r' || cIndex == roundFile.size)
				{
					u32 lineLength = cIndex - lineStartPos;
					const char* linePntr = &fileData[lineStartPos];
					if (lineLength >= 3 && linePntr[1] == ':')
					{
						if (linePntr[0] == 'Q')
						{
							numQuestions++;
							numClues = 0;
							numAnswers = 0;
						}
						else if (linePntr[0] == 'C')
						{
							Assert(numQuestions > 0);
							Assert(numClues < R3_NUM_CLUES_PER_Q);
							numClues++;
						}
						else if (linePntr[0] == 'A')
						{
							Assert(numQuestions > 0);
							Assert(numAnswers == 0);
							numAnswers++;
						}
					}
					lineStartPos = cIndex+1;
				}
			}
		}
		
		//Allocate space for questions
		Assert(game->r3Questions == nullptr);
		Assert(game->r3Clues == nullptr);
		Assert(game->r3Answers == nullptr);
		game->r3NumQuestions = numQuestions;
		game->r3Questions = PushArray(mainHeap, char*, numQuestions); memset(game->r3Questions, 0x00, sizeof(char*)*numQuestions);
		game->r3Clues = PushArray(mainHeap, char*, numQuestions * R3_NUM_CLUES_PER_Q); memset(game->r3Clues, 0x00, sizeof(char*) * (numQuestions*R3_NUM_CLUES_PER_Q));
		game->r3Answers = PushArray(mainHeap, char*, numQuestions); memset(game->r3Answers, 0x00, sizeof(char*)*numQuestions);
		numQuestions = 0;
		numClues = 0;
		numAnswers = 0;
		
		//Load question data
		{
			u32 lineStartPos = 0;
			for (u32 cIndex = 0; cIndex <= roundFile.size; cIndex++)
			{
				char c = (cIndex < roundFile.size) ? fileData[cIndex] : '\n';
				if (c == '\n' || c == '\r' || cIndex == roundFile.size)
				{
					u32 lineLength = cIndex - lineStartPos;
					const char* linePntr = &fileData[lineStartPos];
					if (lineLength >= 3 && linePntr[1] == ':')
					{
						if (linePntr[0] == 'Q')
						{
							Assert(numQuestions < game->r3NumQuestions);
							Assert(game->r3Questions[numQuestions] == nullptr);
							game->r3Questions[numQuestions] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
							numQuestions++;
							numClues = 0;
							numAnswers = 0;
						}
						else if (linePntr[0] == 'C')
						{
							Assert(numQuestions > 0);
							Assert(numClues < R3_NUM_CLUES_PER_Q);
							Assert(game->r3Clues[((numQuestions-1)*R3_NUM_CLUES_PER_Q) + numClues] == nullptr);
							game->r3Clues[((numQuestions-1)*R3_NUM_CLUES_PER_Q) + numClues] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
							numClues++;
						}
						else if (linePntr[0] == 'A')
						{
							Assert(numQuestions > 0 && (numQuestions-1) < game->r3NumQuestions);
							Assert(numAnswers == 0);
							Assert(game->r3Answers[numQuestions-1] == nullptr);
							game->r3Answers[numQuestions-1] = ArenaString(mainHeap, &linePntr[3], lineLength-3);
							numAnswers++;
						}
						else
						{
							DEBUG_PrintLine("Unknown line type %c: \"%.*s\"", linePntr[0], lineLength, linePntr);
						}
					}
					else if (lineLength > 0)
					{
						DEBUG_PrintLine("Unknown line: \"%.*s\"", lineLength, linePntr);
					}
					lineStartPos = cIndex+1;
				}
			}
		}
		
		DEBUG_PrintLine("Loaded Round3 %u questions", numQuestions);
	}
}

void StartQuestionMusic()
{
	if (game->questionMusic == nullptr)
	{
		game->questionMusic = PlayGameSound(&app->thinkingLoop2, true, 1.0f, 1.0f, false);
	}
	else
	{
		ResumeSoundInstance(game->questionMusic);
	}
}
void StopQuestionMusic()
{
	if (game->questionMusic != nullptr)
	{
		DEBUG_WriteLine("Pausing questionMusic");
		PauseSoundInstance(game->questionMusic);
	}
}

void StartCountdownMusic()
{
	if (game->countdownMusic == nullptr)
	{
		game->countdownMusic = PlayGameSound(&app->thinkingLoop2, true, 1.0f, 1.0f, false);
	}
	else
	{
		ResumeSoundInstance(game->countdownMusic);
	}
}
void StopCountdownMusic()
{
	if (game->countdownMusic != nullptr)
	{
		PauseSoundInstance(game->countdownMusic);
	}
}

// +--------------------------------------------------------------+
// |                          Initialize                          |
// +--------------------------------------------------------------+
void InitializeGameState()
{
	GameLoadContent();
	game->timerPaused = true;
	
	game->initialized = true;
}

// +--------------------------------------------------------------+
// |                            Start                             |
// +--------------------------------------------------------------+
void StartGameState(AppState_t fromState)
{
	GameLoadQuestions();
	game->volumeChangeTime = VOLUMES_SHOW_TIME;
}

// +--------------------------------------------------------------+
// |                         Deinitialize                         |
// +--------------------------------------------------------------+
void DeinitializeGameState()
{
	DestroyTexture(&game->testTexture);
	DestroyTexture(&game->circuitTexture);
	DestroyTexture(&game->missingTexture);
	DestroyTexture(&game->canvasTexture);
	if (game->questionMusic != nullptr)
	{
		StopSoundInstance(game->questionMusic);
		game->questionMusic = nullptr;
	}
	if (game->countdownMusic != nullptr)
	{
		StopSoundInstance(game->countdownMusic);
		game->countdownMusic = nullptr;
	}
	if (game->finishMusic != nullptr)
	{
		StopSoundInstance(game->finishMusic);
		game->finishMusic = nullptr;
	}
	
	DeallocateQuestions();
	
	StopAllMusic();
	StopAllSounds();
	ClearPointer(game);
}

// +--------------------------------------------------------------+
// |                         Com Message                          |
// +--------------------------------------------------------------+
void ComMessageGameState(const char* messageStr)
{
	u32 messageLength = (u32)strlen(messageStr);
	if (strncmp(messageStr, "~State", 6) == 0)
	{
		u32 numNumbers = 0;
		u32* numbers = ParseNumbersInString(TempArena, messageStr, messageLength, &numNumbers);
		if (numNumbers == 2)
		{
			u8 buttonIndex = (u8)numbers[0];
			bool buttonPressed = (numbers[1] != 0);
			// +==============================+
			// |       Button 1 Pressed       |
			// +==============================+
			if (buttonIndex == 1 && buttonPressed)
			{
				PlayGameSound(&app->buttonSound, false, 1.0f, 1.0f + RandR32(-0.1f, 0.1f));
				if (game->roundNum == 1 && !game->timerPaused && !game->buttonTriggered && !game->showAnswer)
				{
					if ((game->questionNum >= 4 && !game->switchedSides) || !game->rightTeamTurn)
					{
						DEBUG_WriteLine("Left Team Chimed In!");
						game->buttonTriggered = 0x01;
						game->timerPaused = true;
						game->rightTeamTurn = false;
						StopQuestionMusic();
					}
				}
				if (game->roundNum == 3 && game->showQuestion && !game->timerPaused && !game->rightTeamTurn)
				{
					DEBUG_WriteLine("Left Team Chimed In!");
					game->timerPaused = true;
					StopCountdownMusic();
				}
			}
			// +==============================+
			// |       Button 2 Pressed       |
			// +==============================+
			else if (buttonIndex == 2 && buttonPressed)
			{
				PlayGameSound(&app->buttonSound, false, 1.0f, 1.0f + RandR32(-0.1f, 0.1f));
				if (game->roundNum == 1 && !game->timerPaused && !game->buttonTriggered && !game->showAnswer)
				{
					if ((game->questionNum >= 4 && !game->switchedSides) || game->rightTeamTurn)
					{
						DEBUG_WriteLine("Right Team Chimed In!");
						game->buttonTriggered = 0x02;
						game->timerPaused = true;
						game->rightTeamTurn = true;
						StopQuestionMusic();
					}
				}
				if (game->roundNum == 3 && game->showQuestion && !game->timerPaused && game->rightTeamTurn)
				{
					DEBUG_WriteLine("Right Team Chimed In!");
					game->timerPaused = true;
					StopCountdownMusic();
				}
			}
		}
	}
}

// +--------------------------------------------------------------+
// |                      Update And Render                       |
// +--------------------------------------------------------------+
void UpdateAndRenderGameState()
{
	// +--------------------------------------------------------------+
	// |                            Update                            |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |     Reload Game Content      |
		// +==============================+
		if (ButtonPressed(Button_F5))
		{
			GameLoadContent();
			PlayGameSound(&app->testSound);
		}
		
		if (ButtonPressed(Button_Q))
		{
			PlayGameSound(&app->round1Sound);
		}
		
		if (game->volumeChangeTime < VOLUMES_SHOW_TIME) { game->volumeChangeTime += platform->timeElapsed; } 
		if (ButtonPressed(Button_I)) { game->volumeChangeTime = 0; app->soundsVolume += 0.05f; UpdateSoundVolume(); }
		if (ButtonPressed(Button_K)) { game->volumeChangeTime = 0; app->soundsVolume -= 0.05f; UpdateSoundVolume(); }
		if (ButtonPressed(Button_O)) { game->volumeChangeTime = 0; app->musicVolume += 0.05f; UpdateMusicVolume(); }
		if (ButtonPressed(Button_L)) { game->volumeChangeTime = 0; app->musicVolume -= 0.05f; UpdateMusicVolume(); }
		
		// +==============================+
		// |      Back to Setup Menu      |
		// +==============================+
		if (ButtonPressed(Button_Escape))
		{
			AppChangeState(AppState_Setup, setup->initialized, true);
		}
		
		// +==============================+
		// |         Timer Update         |
		// +==============================+
		if (!game->timerPaused)
		{
			game->fourPointTimeElapsed += (u32)platform->timeElapsed;
			if (game->fourPointTimeElapsed > QUESTION_TIME) { game->fourPointTimeElapsed = QUESTION_TIME; }
			if (game->rightTeamTurn) { DecrementBy(game->countdownTimeRight, (u32)platform->timeElapsed); }
			else { DecrementBy(game->countdownTimeLeft, (u32)platform->timeElapsed); }
			DecrementBy(game->timeTillClue, (u32)platform->timeElapsed);
		}
		
		// +==============================+
		// |      Clue Board Update       |
		// +==============================+
		bool showClueBoard = (game->roundNum == 2 && game->showBoard);
		if (showClueBoard)
		{
			game->clueBoardAnimAmount += 0.02f;
			if (game->clueBoardAnimAmount > 1.0f) { game->clueBoardAnimAmount = 1.0f; }
		}
		else
		{
			game->clueBoardAnimAmount -= 0.04f;
			if (game->clueBoardAnimAmount < 0.0f) { game->clueBoardAnimAmount = 0.0f; }
		}
		for (u32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++)
		{
			if (game->clueRevealed[cIndex])
			{
				game->clueAnimAmounts[cIndex] += 0.03f;
				if (game->clueAnimAmounts[cIndex] > 1.0f) { game->clueAnimAmounts[cIndex] = 1.0f; }
			}
			else
			{
				game->clueAnimAmounts[cIndex] -= 0.05f;
				if (game->clueAnimAmounts[cIndex] < 0.0f) { game->clueAnimAmounts[cIndex] = 0.0f; }
			}
		}
		
		// +==============================+
		// |      Four Point Update       |
		// +==============================+
		if (game->roundNum == 1)
		{
			game->fourPointAnimAmount += 0.04f;
			if (game->fourPointAnimAmount > 1.0f) { game->fourPointAnimAmount = 1.0f; }
		}
		else
		{
			game->fourPointAnimAmount -= 0.04f;
			if (game->fourPointAnimAmount < 0.0f) { game->fourPointAnimAmount = 0.0f; }
		}
		
		// +==============================+
		// |    Countdown Anim Update     |
		// +==============================+
		if (game->showCountdown)
		{
			game->countdownAnimAmount += 0.03f;
			if (game->countdownAnimAmount > 1.0f) { game->countdownAnimAmount = 1.0f; }
		}
		else
		{
			game->countdownAnimAmount -= 0.03f;
			if (game->countdownAnimAmount < 0.0f) { game->countdownAnimAmount = 0.0f; }
		}
		
		// +==============================+
		// |    Countdown Anim Update     |
		// +==============================+
		if (game->roundNum == 4)
		{
			game->winTextAnimAmount += 0.02f;
			if (game->winTextAnimAmount > 1.0f) { game->winTextAnimAmount = 1.0f; }
		}
		else
		{
			game->winTextAnimAmount -= 0.02f;
			if (game->winTextAnimAmount < 0.0f) { game->winTextAnimAmount = 0.0f; }
		}
		
		// +==============================+
		// |         Update Tray          |
		// +==============================+
		UpdateTray();
		
		// +==============================+
		// |      Debug Change Round      |
		// +==============================+
		if (ButtonPressed(Button_Plus) || (game->roundNum == 0 && ButtonPressedUnhandled(Button_Enter)) || ButtonPressed(Button_Minus))
		{
			if (ButtonPressed(Button_Plus) || (game->roundNum == 0 && ButtonPressedUnhandled(Button_Enter)))
			{
				HandleButton(Button_Enter);
				game->roundNum++;
				if (game->roundNum > 4) { game->roundNum = 0; }
				if (game->roundNum == 1) { PlayGameSound(&app->round1Sound); }
			}
			else
			{
				if (game->roundNum > 0) { game->roundNum--; }
				else { game->roundNum = 4; }
				StopAllMusic();
				StopAllSounds();
				game->questionMusic = nullptr;
				game->countdownMusic = nullptr;
				game->finishMusic = nullptr;
			}
			
			game->questionStr = nullptr;
			game->answerStr = nullptr;
			game->questionNum = 0;
			game->numCluesShown = 0;
			game->buttonTriggered = 0;
			game->switchedSides = false;
			game->rightTeamTurn = false;
			game->showQuestion = false;
			game->timerPaused = true;
			game->answeredCorrectly = true;
			game->showAnswer = false;
			game->showBoard = false;
			game->showCountdown = false;
			game->countdownTimeLeft = 0;
			game->countdownTimeRight = 0;
		}
		
		// +==============================+
		// |        Update Round 1        |
		// +==============================+
		if (game->roundNum == 1)
		{
			Assert(game->questionNum < R1_NUM_QUESTIONS);
			
			u8 numPoints = (u8)CeilR32(game->fourPointSliderAmount * 4);
			u8 numClues = 4 - numPoints;
			if (!game->showQuestion)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					Assert(game->questionNum < R1_NUM_QUESTIONS);
					game->showQuestion = true;
					game->fourPointTimeElapsed = 0;
					game->buttonTriggered = 0x00;
					game->numCluesShown = 0;
					game->rightTeamTurn = ((game->questionNum%2) != 0);
					game->timerPaused = true;
					game->switchedSides = false;
					game->questionStr = game->r1Questions[game->questionNum];
					game->answerStr = game->r1Answers[game->questionNum];
					
					DEBUG_PrintLine("Showing question %u/%u: %s -> %s", game->questionNum+1, R1_NUM_QUESTIONS, game->questionStr, game->answerStr);
				}
			}
			else if (game->showAnswer)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					game->showAnswer = false;
					game->showQuestion = false;
					game->questionNum++;
					game->fourPointTimeElapsed = 0;
					
					// +==============================+
					// |         Goto Round 2         |
					// +==============================+
					if (game->questionNum >= R1_NUM_QUESTIONS)
					{
						DEBUG_WriteLine("Going to round 2!");
						game->roundNum = 2;
						game->questionStr = nullptr;
						game->answerStr = nullptr;
						game->questionNum = 0;
						game->numCluesShown = 0;
						game->buttonTriggered = 0;
						game->switchedSides = false;
						game->rightTeamTurn = false;
						game->showQuestion = false;
						game->timerPaused = true;
						game->answeredCorrectly = true;
						game->showAnswer = false;
						game->showBoard = false;
					}
					else
					{
						DEBUG_WriteLine("Going to next question");
					}
				}
			}
			else if (game->timerPaused && !game->buttonTriggered)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					DEBUG_WriteLine("Starting timer...");
					game->timerPaused = false;
					StartQuestionMusic();
				}
			}
			else if (!game->timerPaused)
			{
				if (game->fourPointSliderAmount <= 0)
				{
					DEBUG_WriteLine("Ran out of time!");
					game->showAnswer = true;
					game->answeredCorrectly = false;
					StopQuestionMusic();
					PlayGameSound(&app->failureSound);
				}
				else
				{
					if (numClues >= 1 && game->numCluesShown < 1)
					{
						DEBUG_PrintLine("===[CLUE 1]===: %s", game->r1Clues[(game->questionNum*R1_NUM_CLUES_PER_Q) + 0]);
						game->numCluesShown = 1;
					}
					if (numClues >= 2 && game->numCluesShown < 2)
					{
						DEBUG_PrintLine("===[CLUE 2]===: %s", game->r1Clues[(game->questionNum*R1_NUM_CLUES_PER_Q) + 1]);
						game->numCluesShown = 2;
					}
					if (numClues >= 3 && game->numCluesShown < 3)
					{
						DEBUG_PrintLine("===[CLUE 3]===: %s", game->r1Clues[(game->questionNum*R1_NUM_CLUES_PER_Q) + 2]);
						game->numCluesShown = 3;
					}
				}
			}
			else if (game->buttonTriggered)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					game->buttonTriggered = 0;
					game->showAnswer = true;
					game->answeredCorrectly = true;
					if (!game->rightTeamTurn)
					{
						DEBUG_PrintLine("Left team scores %u points!", numPoints);
						game->scoreLeft += numPoints;
					}
					else
					{
						DEBUG_PrintLine("Right team scores %u points!", numPoints);
						game->scoreRight += numPoints;
					}
					PlayGameSound(&app->successSound);
				}
				else if (ButtonPressedUnhandled(Button_Backspace))
				{
					HandleButton(Button_Backspace);
					DEBUG_WriteLine("Incorrect answer!");
					game->buttonTriggered = 0;
					if (!game->switchedSides)
					{
						DEBUG_PrintLine("Giving %s Team a chance...", game->rightTeamTurn ? "Left" : "Right");
						game->answeredCorrectly = false;
						game->rightTeamTurn = !game->rightTeamTurn;
						game->switchedSides = true;
					}
					else
					{
						game->showAnswer = true;
						game->answeredCorrectly = false;
					}
					PlayGameSound(&app->failureSound);
				}
			}
			
		}
		
		// +==============================+
		// |        Update Round 2        |
		// +==============================+
		else if (game->roundNum == 2)
		{
			if (!game->showBoard)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					Assert(game->questionNum < R2_NUM_QUESTIONS);
					game->showBoard = true;
					game->showQuestion = false;
					game->showAnswer = false;
					game->potPointsLeft = 5;
					game->potPointsRight = 5;
					game->rightTeamTurn = ((game->questionNum%2) == 0);
					game->numCluesShown = 0;
					game->answerStr = game->r2Answers[game->questionNum];
					DEBUG_PrintLine("Showing clue board %u/%u for answer: %s", game->questionNum+1, R2_NUM_QUESTIONS, game->answerStr);
				}
			}
			else if (!game->showQuestion)
			{
				// +==============================+
				// |      Check Clue Clicked      |
				// +==============================+
				for (u32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++)
				{
					rec clueRec = game->clueRecs[cIndex];
					if (ClickedOnRec(clueRec))
					{
						game->clueRevealed[cIndex] = !game->clueRevealed[cIndex];
						game->showQuestion = true;
						game->numCluesShown++;
					}
				}
			}
			else if (!game->showAnswer)
			{
				if (ButtonPressedUnhandled(Button_Enter) || ButtonPressedUnhandled(Button_Backspace))
				{
					if (ButtonPressedUnhandled(Button_Enter))
					{
						HandleButton(Button_Enter);
						game->showAnswer = true;
						game->answeredCorrectly = true;
						if (!game->rightTeamTurn)
						{
							DEBUG_PrintLine("Left team scores %u points!", game->potPointsLeft);
							game->scoreLeft += game->potPointsLeft;
						}
						else
						{
							DEBUG_PrintLine("Right team scores %u points!", game->potPointsRight);
							game->scoreRight += game->potPointsRight;
						}
						PlayGameSound(&app->successSound);
					}
					else
					{
						HandleButton(Button_Backspace);
						DEBUG_WriteLine("Incorrect answer!");
						if (!game->rightTeamTurn)
						{
							if (game->potPointsLeft > 1) { game->potPointsLeft--; }
						}
						else
						{
							if (game->potPointsRight > 1) { game->potPointsRight--; }
						}
						
						game->showQuestion = false;
						game->rightTeamTurn = !game->rightTeamTurn;
						
						if (game->numCluesShown >= R2_NUM_CLUES_PER_Q)
						{
							DEBUG_WriteLine("That's all the clues. Both teams lose!");
							game->showAnswer = true;
							game->answeredCorrectly = false;
						}
						else
						{
							DEBUG_PrintLine("Passing it over to %s Team...", game->rightTeamTurn ? "Right" : "Left");
						}
						
						PlayGameSound(&app->failureSound);
					}
					
				}
			}
			else //showing answer
			{
				for (u32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++) { game->clueRevealed[cIndex] = true; }
				
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					game->questionNum++;
					// +==============================+
					// |         Goto Round 3         |
					// +==============================+
					if (game->questionNum >= R2_NUM_QUESTIONS)
					{
						DEBUG_WriteLine("Going to round 3!");
						game->roundNum = 3;
						game->questionStr = nullptr;
						game->answerStr = nullptr;
						game->questionNum = 0;
						game->numCluesShown = 0;
						game->buttonTriggered = 0;
						game->switchedSides = false;
						game->rightTeamTurn = false;
						game->showQuestion = false;
						game->timerPaused = true;
						game->answeredCorrectly = true;
						game->showAnswer = false;
						game->showBoard = false;
						game->showCountdown = false;
					}
					else
					{
						DEBUG_WriteLine("On to the next question...");
						game->showBoard = false;
						game->showAnswer = false;
						for (u32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++) { game->clueRevealed[cIndex] = false; }
					}
				}
			}
		}
		
		// +==============================+
		// |        Update Round 3        |
		// +==============================+
		else if (game->roundNum == 3)
		{
			if (!game->showCountdown)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					if (game->scoreLeft >= game->scoreRight)
					{
						u32 numPointsAhead = game->scoreLeft - game->scoreRight;
						game->countdownTimeLeft = (R3_BASE_TIME + numPointsAhead*R3_TIME_PER_POINT) * 1000;
						game->countdownTimeRight = R3_BASE_TIME * 1000;
						game->rightTeamTurn = true;
						DEBUG_PrintLine("Left team is ahead by %u points. %us vs %us", numPointsAhead, game->countdownTimeLeft/1000, game->countdownTimeRight/1000);
					}
					else
					{
						u32 numPointsAhead = game->scoreRight - game->scoreLeft;
						game->countdownTimeLeft = R3_BASE_TIME * 1000;
						game->countdownTimeRight = (R3_BASE_TIME + numPointsAhead*R3_TIME_PER_POINT) * 1000;
						game->rightTeamTurn = false;
						DEBUG_PrintLine("Right team is ahead by %u points. %us vs %us", numPointsAhead, game->countdownTimeLeft/1000, game->countdownTimeRight/1000);
					}
					
					game->timerPaused = true;
					game->showCountdown = true;
					game->showQuestion = false;
					game->questionNum = 0;
				}
			}
			else if (!game->showQuestion)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					
					game->questionStr = game->r3Questions[game->questionNum];
					game->answerStr = game->r3Answers[game->questionNum];
					game->showQuestion = true;
					game->timerPaused = false;
					game->numCluesShown = 0;
					game->timeTillClue = R3_CLUE_TIME*1000;
					DEBUG_PrintLine("Showing question %u/%u: %s -> %s", game->questionNum+1, game->r3NumQuestions, game->questionStr, game->answerStr);
					StartCountdownMusic();
				}
			}
			else if (!game->timerPaused && !game->showAnswer)
			{
				if (!game->rightTeamTurn)
				{
					if (game->countdownTimeLeft == 0)
					{
						DEBUG_WriteLine("Left Team ran out of time!");
						game->showAnswer = true;
						game->answeredCorrectly = false;
						StopCountdownMusic();
						PlayGameSound(&app->failureSound);
					}
				}
				else
				{
					if (game->countdownTimeRight == 0)
					{
						DEBUG_WriteLine("Right Team ran out of time!");
						game->showAnswer = true;
						game->answeredCorrectly = false;
						StopCountdownMusic();
						PlayGameSound(&app->failureSound);
					}
				}
				
				if (game->timeTillClue == 0 && game->numCluesShown < R3_NUM_CLUES_PER_Q)
				{
					char* clueStr = game->r3Clues[(game->questionNum*R3_NUM_CLUES_PER_Q) + game->numCluesShown];
					DEBUG_PrintLine("===[CLUE %u]===: %s", game->numCluesShown+1, clueStr);
					game->numCluesShown++;
					game->timeTillClue = R3_CLUE_TIME*1000;
				}
			}
			else if (!game->showAnswer)
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					DEBUG_WriteLine("Answered correctly!");
					game->showAnswer = true;
					game->answeredCorrectly = true;
					PlayGameSound(&app->successSound);
				}
				else if (ButtonPressedUnhandled(Button_Backspace))
				{
					HandleButton(Button_Enter);
					DEBUG_WriteLine("Answered incorrectly!");
					game->showAnswer = true;
					game->answeredCorrectly = false;
					PlayGameSound(&app->failureSound);
				}
			}
			else
			{
				if (ButtonPressedUnhandled(Button_Enter))
				{
					HandleButton(Button_Enter);
					
					u32* teamTimeLeft = (game->rightTeamTurn ? &game->countdownTimeRight : &game->countdownTimeLeft);
					if (game->answeredCorrectly && *teamTimeLeft > 0)
					{
						game->questionNum++;
						game->rightTeamTurn = !game->rightTeamTurn;
					}
					else
					{
						game->questionNum++;
						if (*teamTimeLeft > R3_PENALTY_TIME*1000) { *teamTimeLeft -= R3_PENALTY_TIME*1000; }
						else { *teamTimeLeft = 0; }
					}
					
					game->showQuestion = false;
					game->showAnswer = false;
					
					// +==============================+
					// |         Goto Round 4         |
					// +==============================+
					if (*teamTimeLeft == 0)
					{
						DEBUG_PrintLine("Round 3 is over! %s team wins", game->rightTeamTurn ? "Left" : "Right");
						game->rightTeamWins = !game->rightTeamTurn;
						game->roundNum = 4;
						game->finishSound = PlayGameSound(&app->finishSound);
					}
					else if (game->questionNum >= game->r3NumQuestions)
					{
						DEBUG_PrintLine("We went through all %u questions. %s team wins!", game->r3NumQuestions, (game->countdownTimeRight >= game->countdownTimeLeft) ? "Right" : "Left");
						game->rightTeamWins = (game->countdownTimeRight >= game->countdownTimeLeft);
						game->roundNum = 4;
						game->finishSound = PlayGameSound(&app->finishSound);
					}
				}
			}
		}
		
		// +==============================+
		// |        Update Round 4        |
		// +==============================+
		else if (game->roundNum == 4)
		{
			if (game->finishSound != nullptr)
			{
				if (!game->finishSound->playing)
				{
					game->finishSound = nullptr;
					game->finishMusic = PlayGameSound(&app->finishSong, false, 1.0f, 1.0f, false);
				}
			}
			
			if (game->finishMusic != nullptr && !game->finishMusic->playing)
			{
				game->finishMusic = nullptr;
			}
		}
		
		// +==============================+
		// |    Check On Button States    |
		// +==============================+
		if (app->button1Down)
		{
			game->buttonLeftLastPress = platform->programTime;
		}
		if (app->button2Down)
		{
			game->buttonRightLastPress = platform->programTime;
		}
	}
	
	// +--------------------------------------------------------------+
	// |                            Render                            |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |            Setup             |
		// +==============================+
		{
			RcBegin(&app->defaultShader, &app->defaultFont, NewRec(Vec2_Zero, RenderScreenSize));
			RcClearColorBuffer(NewColor(Color_Lime));
			RcClearDepthBuffer(1.0f);
			RcBindNewFont(&app->uiFont);
			RcSetFontSize(0);
			RcSetFontAlignment(Alignment_Center);
			RcSetFontStyle(FontStyle_Default);
		}
		
		// +==============================+
		// |   Draw Background Vignette   |
		// +==============================+
		RcEnableVignette(0.6f, 0.7f);
		RcBindTexture(&game->canvasTexture);
		RcDrawTexturedRec(NewRec(Vec2_Zero, RenderScreenSize), NewColor(Color_SlateBlue), NewRec(0, 0, RenderScreenSize.width, RenderScreenSize.height));
		RcDisableVignette();
		
		// +==============================+
		// |           Test UI            |
		// +==============================+
		r32 scoreLineHeight    = (r32)FontGetLineHeight(&app->scoreFont);
		r32 scoreMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->scoreFont);
		r32 scoreMaxExtendDown = (r32)FontGetMaxExtendDown(&app->scoreFont);
		
		r32 questionLineHeight    = (r32)FontGetLineHeight(&app->questionFont);
		r32 questionMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->questionFont);
		r32 questionMaxExtendDown = (r32)FontGetMaxExtendDown(&app->questionFont);
		
		r32 uiLineHeight    = (r32)FontGetLineHeight(&app->uiFont);
		r32 uiMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->uiFont);
		r32 uiMaxExtendDown = (r32)FontGetMaxExtendDown(&app->uiFont);
		
		r32 clueLineHeight    = (r32)FontGetLineHeight(&app->clueFont);
		r32 clueMaxExtendUp   = (r32)FontGetMaxExtendUp(&app->clueFont);
		r32 clueMaxExtendDown = (r32)FontGetMaxExtendDown(&app->clueFont);
		
		// +==============================+
		// |      Four Piece Slider       |
		// +==============================+
		{
			RcDrawRectangle(RecInflate(game->fourPointRec, 2), TrayColor);
			RcDrawGradient(game->fourPointRec, TrayColorDark, TrayColor, Dir2_Down);
			for (u32 pIndex = 0; pIndex < 4; pIndex++)
			{
				RcBindNewFont(&app->uiFont);
				if (game->fourPointSliderAmount > 0 && game->fourPointSliderAmount*4 >= pIndex)
				{
					RcSetFontStyle(FontStyle_Bold);
					RcDrawGradient(game->fourPointPieceRecs[pIndex], SliderColor1, SliderColor2, Dir2_Down);
				}
				else
				{
					RcSetFontStyle(FontStyle_Default);
				}
				RcNewPrintString(game->fourPointTextPos[pIndex], ScoreColor, "%u POINT%s", pIndex+1, (pIndex > 0) ? "S" : "");
			}
			for (u32 pIndex = 1; pIndex < 4; pIndex++)
			{
				rec dividerRec = game->fourPointPieceRecs[pIndex];
				dividerRec.width = FOUR_POINT_DIVIDER_WIDTH;
				dividerRec.x -= dividerRec.width/2;
				RcDrawRectangle(dividerRec, TrayColorLight);
			}
			RcDrawButton(game->fourPointRec, ColorTransparent(0), NewColor(Color_Black), 1);
			glBlendFunc(GL_ONE, GL_ONE);
			RcDrawRectangle(game->fourPointSliderRec, ColorTransparent(SliderColor2, 0.6f));
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			
			Color_t timeColor = ColorTransparent(NewColor(Color_White), game->fourPointAnimAmount);
			RcBindNewFont(&app->uiFont);
			RcSetFontStyle(FontStyle_Bold);
			RcNewPrintString(game->fourPointSliderRec.topLeft + NewVec2(game->fourPointSliderRec.width/2, -(2 + uiMaxExtendDown)), timeColor, "%.1f", (QUESTION_TIME - game->fourPointTimeElapsed) / 1000.f);
		}
		
		// +==============================+
		// |             Tray             |
		// +==============================+
		RcDrawRectangle(game->trayRec, TrayColor);
		RcDrawGradient(NewRec(game->trayRec.x, game->trayRec.y, game->trayRec.width, game->trayRec.height/4), TrayColorLight, TrayColor, Dir2_Down);
		RcDrawGradient(NewRec(game->trayRec.x, game->trayRec.y + game->trayRec.height*3/4, game->trayRec.width, game->trayRec.height/4), TrayColor, TrayColorDark, Dir2_Down);
		
		// +==============================+
		// |          Left Side           |
		// +==============================+
		{
			rec scoreLeftSliceHorRec = game->scoreLeftRec;
			scoreLeftSliceHorRec.height = 2;
			scoreLeftSliceHorRec.y += game->scoreLeftRec.height/2;
			scoreLeftSliceHorRec.topLeft = Vec2Round(scoreLeftSliceHorRec.topLeft);
			rec scoreLeftSliceVertRec = game->scoreLeftRec;
			scoreLeftSliceVertRec.width = 2;
			scoreLeftSliceVertRec.x += game->scoreLeftRec.width/2;
			scoreLeftSliceVertRec.topLeft = Vec2Round(scoreLeftSliceVertRec.topLeft);
			
			bool isLeftTurn = (!game->rightTeamTurn || (game->roundNum == 1 && game->questionNum >= 4 && !game->switchedSides && (game->buttonTriggered == 0 || game->buttonTriggered == 1)));
			bool showButtonBackRec = (game->roundNum == 2 || (game->roundNum == 3 && !game->rightTeamTurn));
			if (game->roundNum == 1)
			{
				if (game->questionNum >= 4 && !game->switchedSides)
				{
					if (game->buttonTriggered == 0 || game->buttonTriggered == 1) { showButtonBackRec = true; }
				}
				else if (!game->rightTeamTurn) { showButtonBackRec = true; }
			}
			if (showButtonBackRec)
			{
				RcEnableVignette(0.8f, 0.8f);
				if (game->buttonTriggered == 1)
				{
					RcDrawRectangle(game->pointStackLeftRec, LeftColor);
				}
				else
				{
					RcDrawRectangle(game->pointStackLeftRec, LeftColorDark);
				}
				RcDisableVignette();
			}
			if (game->roundNum == 2 && game->showBoard)
			{
				for (u32 pIndex = 0; pIndex < game->potPointsLeft; pIndex++)
				{
					RcDrawGradient(RecDeflateY(game->pointLeftBaseRec - NewVec2(0, game->pointLeftBaseRec.height*pIndex), 1), LeftColor, LeftColorDark, Dir2_Down);
				}
			}
			rec leftTurnRec = NewRec(game->trayRec.topLeft, Vec2_Zero);
			leftTurnRec.x = 0;
			leftTurnRec.width = game->trayRec.x;
			leftTurnRec.height = RenderScreenSize.height - leftTurnRec.y;
			leftTurnRec = RecSquarify(leftTurnRec);
			leftTurnRec = RecDeflate(leftTurnRec, 20);
			if (isLeftTurn && (game->roundNum >= 1 && game->roundNum <= 3) && (game->showQuestion || game->roundNum == 2) && !game->showAnswer)
			{
				Color_t turnSpriteColor = LeftColor;
				if (game->buttonLeftLastPress != 0 && TimeSince(game->buttonLeftLastPress) < BUTTON_PRESS_FADE_TIME)
				{
					u64 timeSincePress = TimeSince(game->buttonLeftLastPress);
					r32 progress = (r32)timeSincePress / BUTTON_PRESS_FADE_TIME;
					turnSpriteColor = ColorLerp(NewColor(Color_White), turnSpriteColor, progress);
				}
				RcBindTexture(&app->turnSprite);
				RcDrawTexturedRec(leftTurnRec, turnSpriteColor);
			}
			
			// RcDrawButton(game->pointStackLeftRec, ColorTransparent(0), NewColor(Color_Black), 1);
			
			if (game->countdownAnimAmount > 0.0f)
			{
				RcEnableVignette(0.6f, 0.5f);
				RcDrawRectangle(game->scoreLeftRec, LeftColor);
				RcDisableVignette();
				
				char* countdownStr = nullptr;
				u32 numSeconds = game->countdownTimeLeft/1000;
				u32 numTenths = (game->countdownTimeLeft%1000) / 100;
				if (game->countdownTimeLeft > 60*1000)
				{
					countdownStr = TempPrint("%u:%02u", numSeconds/60, numSeconds%60);
				}
				else
				{
					countdownStr = TempPrint("%u.%u", numSeconds, numTenths);
				}
				v2 textPos = game->scoreLeftRec.topLeft + game->scoreLeftRec.size/2;
				textPos.y += scoreMaxExtendUp - scoreLineHeight/2;
				RcBindNewFont(&app->scoreFont);
				RcNewDrawNtString(countdownStr, textPos, ScoreColor);
			}
			
			rec quarterRec = NewRec(game->scoreLeftRec.topLeft, game->scoreLeftRec.size/2);
			if (game->countdownAnimAmount < 1.0f)
			{
				for (u8 yPos = 0; yPos < 2; yPos++) { for (u8 xPos = 0; xPos < 2; xPos++)
				{
					rec tileRec = quarterRec;
					tileRec.x += quarterRec.width * (r32)xPos;
					tileRec.y += quarterRec.height * (r32)yPos;
					rec scoreRec = game->scoreLeftRec;
					r32 offset = 0;
					if (yPos == 0) { offset = -game->scoreRightRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount); }
					else        { offset =  game->scoreRightRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount); }
					tileRec.y += offset;
					scoreRec.y += offset;
					RcSetViewport(RecOverlap(tileRec, game->scoreLeftRec));
					RcEnableVignette(0.6f, 0.5f);
					RcDrawRectangle(scoreRec, LeftColor);
					RcDisableVignette();
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.1f, 0.1f)), 3, ColorTransparent(0.2f));
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.9f, 0.1f)), 3, ColorTransparent(0.2f));
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.1f, 0.9f)), 3, ColorTransparent(0.2f));
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.9f, 0.9f)), 3, ColorTransparent(0.2f));
					RcBindNewFont(&app->scoreFont);
					RcNewPrintString((yPos == 0) ? game->scoreLeftTextPos1 : game->scoreLeftTextPos2, ScoreColor, "%u", game->scoreLeft);
					RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
				} }
			}
			
			if (game->buttonLeftLastPress != 0 && TimeSince(game->buttonLeftLastPress) < BUTTON_PRESS_FADE_TIME)
			{
				u64 timeSincePress = TimeSince(game->buttonLeftLastPress);
				r32 progress = (r32)timeSincePress / BUTTON_PRESS_FADE_TIME;
				RcDrawButton(game->scoreLeftRec, ColorTransparent(NewColor(Color_White), (1-progress)*0.5f), ColorTransparent(NewColor(Color_White), 1-progress), 2);
			}
			
			if (game->countdownAnimAmount == 0)
			{
				RcDrawRectangle(scoreLeftSliceHorRec, NewColor(Color_Black));
				RcDrawRectangle(scoreLeftSliceVertRec, NewColor(Color_Black));
			}
		}
		
		// +==============================+
		// |          Right Side          |
		// +==============================+
		{
			rec scoreRightSliceHorRec = game->scoreRightRec;
			scoreRightSliceHorRec.height = 2;
			scoreRightSliceHorRec.y += game->scoreRightRec.height/2;
			scoreRightSliceHorRec.topLeft = Vec2Round(scoreRightSliceHorRec.topLeft);
			rec scoreRightSliceVertRec = game->scoreRightRec;
			scoreRightSliceVertRec.width = 2;
			scoreRightSliceVertRec.x += game->scoreRightRec.width/2;
			scoreRightSliceVertRec.topLeft = Vec2Round(scoreRightSliceVertRec.topLeft);
			
			bool isRightTurn = (game->rightTeamTurn || (game->roundNum == 1 && game->questionNum >= 4 && !game->switchedSides && (game->buttonTriggered == 0 || game->buttonTriggered == 2)));
			bool showButtonBackRec = (game->roundNum == 2 || (game->roundNum == 3 && game->rightTeamTurn));
			if (game->roundNum == 1)
			{
				if (game->questionNum >= 4 && !game->switchedSides)
				{
					if (game->buttonTriggered == 0 || game->buttonTriggered == 2) { showButtonBackRec = true; }
				}
				else if (game->rightTeamTurn) { showButtonBackRec = true; }
			}
			if (showButtonBackRec)
			{
				RcEnableVignette(0.8f, 0.8f);
				if (game->buttonTriggered == 2)
				{
					RcDrawRectangle(game->pointStackRightRec, RightColor);
				}
				else
				{
					RcDrawRectangle(game->pointStackRightRec, RightColorDark);
				}
				RcDisableVignette();
			}
			if (game->roundNum == 2 && game->showBoard)
			{
				for (u32 pIndex = 0; pIndex < game->potPointsRight; pIndex++)
				{
					RcDrawGradient(RecDeflateY(game->pointRightBaseRec - NewVec2(0, game->pointRightBaseRec.height*pIndex), 1), RightColor, RightColorDark, Dir2_Down);
				}
			}
			rec rightTurnRec = NewRec(game->trayRec.topLeft, Vec2_Zero);
			rightTurnRec.x += game->trayRec.width;
			rightTurnRec.width = RenderScreenSize.width - rightTurnRec.x;
			rightTurnRec.height = RenderScreenSize.height - rightTurnRec.y;
			rightTurnRec = RecSquarify(rightTurnRec);
			rightTurnRec = RecDeflate(rightTurnRec, 20);
			if (isRightTurn && (game->roundNum >= 1 && game->roundNum <= 3) && (game->showQuestion || game->roundNum == 2) && !game->showAnswer)
			{
				Color_t turnSpriteColor = RightColor;
				if (game->buttonRightLastPress != 0 && TimeSince(game->buttonRightLastPress) < BUTTON_PRESS_FADE_TIME)
				{
					u64 timeSincePress = TimeSince(game->buttonRightLastPress);
					r32 progress = (r32)timeSincePress / BUTTON_PRESS_FADE_TIME;
					turnSpriteColor = ColorLerp(NewColor(Color_White), turnSpriteColor, progress);
				}
				RcBindTexture(&app->turnSprite);
				RcDrawTexturedRec(rightTurnRec, turnSpriteColor);
			}
			
			// RcDrawButton(game->pointStackRightRec, ColorTransparent(0), NewColor(Color_Black), 1);
			
			if (game->countdownAnimAmount > 0.0f)
			{
				RcEnableVignette(0.6f, 0.5f);
				RcDrawRectangle(game->scoreRightRec, RightColor);
				RcDisableVignette();
				
				char* countdownStr = nullptr;
				u32 numSeconds = game->countdownTimeRight/1000;
				u32 numTenths = (game->countdownTimeRight%1000) / 100;
				if (game->countdownTimeRight > 60*1000)
				{
					countdownStr = TempPrint("%u:%02u", numSeconds/60, numSeconds%60);
				}
				else
				{
					countdownStr = TempPrint("%u.%u", numSeconds, numTenths);
				}
				v2 textPos = game->scoreRightRec.topLeft + game->scoreRightRec.size/2;
				textPos.y += scoreMaxExtendUp - scoreLineHeight/2;
				RcBindNewFont(&app->scoreFont);
				RcNewDrawNtString(countdownStr, textPos, ScoreColor);
			}
			
			rec quarterRec = NewRec(game->scoreRightRec.topLeft, game->scoreRightRec.size/2);
			if (game->countdownAnimAmount < 1.0f)
			{
				for (u8 yPos = 0; yPos < 2; yPos++) { for (u8 xPos = 0; xPos < 2; xPos++)
				{
					rec tileRec = quarterRec;
					tileRec.x += quarterRec.width * (r32)xPos;
					tileRec.y += quarterRec.height * (r32)yPos;
					rec scoreRec = game->scoreRightRec;
					r32 offset = 0;
					if (yPos == 0) { offset = -game->scoreRightRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount); }
					else        { offset =  game->scoreRightRec.height/2 * Ease(EasingStyle_QuadraticIn, game->countdownAnimAmount); }
					tileRec.y += offset;
					scoreRec.y += offset;
					RcSetViewport(RecOverlap(tileRec, game->scoreRightRec));
					RcEnableVignette(0.6f, 0.5f);
					RcDrawRectangle(scoreRec, RightColor);
					RcDisableVignette();
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.1f, 0.1f)), 3, ColorTransparent(0.2f));
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.9f, 0.1f)), 3, ColorTransparent(0.2f));
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.1f, 0.9f)), 3, ColorTransparent(0.2f));
					RcDrawCircle(tileRec.topLeft + Vec2Multiply(tileRec.size, NewVec2(0.9f, 0.9f)), 3, ColorTransparent(0.2f));
					RcBindNewFont(&app->scoreFont);
					RcNewPrintString((yPos == 0) ? game->scoreRightTextPos1 : game->scoreRightTextPos2, ScoreColor, "%u", game->scoreRight);
					RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
				} }
			}
			
			if (game->buttonRightLastPress != 0 && TimeSince(game->buttonRightLastPress) < BUTTON_PRESS_FADE_TIME)
			{
				u64 timeSincePress = TimeSince(game->buttonRightLastPress);
				r32 progress = (r32)timeSincePress / BUTTON_PRESS_FADE_TIME;
				RcDrawButton(game->scoreRightRec, ColorTransparent(NewColor(Color_White), (1-progress)*0.5f), ColorTransparent(NewColor(Color_White), 1-progress), 2);
			}
			
			if (game->countdownAnimAmount == 0)
			{
				RcDrawRectangle(scoreRightSliceHorRec, NewColor(Color_Black));
				RcDrawRectangle(scoreRightSliceVertRec, NewColor(Color_Black));
			}
		}
		
		// +==============================+
		// |         Question Rec         |
		// +==============================+
		{
			RcEnableVignette(0.5f, 0.5f);
			RcDrawRectangle(game->questionRec, QuestionWindowColor);
			RcDisableVignette();
			RcSetViewport(game->questionRec);
			
			RcBindNewFont(&app->scoreFont);
			RcNewDrawNtString("LATERAL", game->lateralLogoPos, ColorTransparent(0.1f));
			
			bool panelsClosing = false;
			if ((game->roundNum == 1 || game->roundNum == 3) && game->showQuestion && !game->showAnswer)
			{
				panelsClosing = false;
				if (game->panelsAnimAmount < 1.0f)
				{
					game->panelsAnimAmount += 0.05f;
					if (game->panelsAnimAmount >= 1.0f) { game->panelsAnimAmount = 1.0f; }
				}
			}
			else
			{
				panelsClosing = true;
				if (game->panelsAnimAmount > 0.0f)
				{
					game->panelsAnimAmount -= 0.05f;
					if (game->panelsAnimAmount <= 0.0f) { game->panelsAnimAmount = 0.0f; }
				}
			}
			
			bool answerShowing = false;
			if ((game->roundNum == 1 || game->roundNum == 2 || game->roundNum == 3) && game->showAnswer)
			{
				answerShowing = true;
				if (game->answerAnimAmount < 1.0f)
				{
					game->answerAnimAmount += 0.05f;
					if (game->answerAnimAmount >= 1.0f) { game->answerAnimAmount = 1.0f; }
				}
			}
			else
			{
				answerShowing = false;
				if (game->answerAnimAmount > 0.0f)
				{
					game->answerAnimAmount -= 0.05f;
					if (game->answerAnimAmount <= 0.0f) { game->answerAnimAmount = 0.0f; }
				}
			}
			
			i32 numPanels = CeilR32(game->questionRec.width / (game->questionRec.height/2));
			rec panelBaseRec = game->questionRec;
			panelBaseRec.size = NewVec2(game->questionRec.height/2);
			panelBaseRec.x -= ((numPanels * panelBaseRec.width) - game->questionRec.width) / 2;
			for (i32 pIndex = 0; pIndex < numPanels; pIndex++)
			{
				r32 panelTime = panelsClosing ? EaseCubicOut(game->panelsAnimAmount) : EaseCubicIn(game->panelsAnimAmount);
				rec panelTopRec = panelBaseRec + Vec2Multiply(panelBaseRec.size, NewVec2((r32)pIndex, 0));
				panelTopRec.y -= panelTopRec.height * (1 - panelTime);
				rec panelBottomRec = panelBaseRec + Vec2Multiply(panelBaseRec.size, NewVec2((r32)pIndex, 1));
				panelBottomRec.y += panelBottomRec.height * (1 - panelTime);
				RcDrawRectangle(panelTopRec, QuestionWindowColor);
				RcDrawGradient(RecDeflate(panelTopRec, 2), PanelColor1, PanelColor2, Dir2_Down);
				RcDrawRectangle(panelBottomRec, QuestionWindowColor);
				RcDrawGradient(RecDeflate(panelBottomRec, 2), PanelColor2, PanelColor1, Dir2_Down);
			}
			
			RcBindNewFont(&app->questionFont);
			Color_t questionColor = ColorTransparent(ScoreColor, panelsClosing ? EaseCubicOut(game->panelsAnimAmount) : EaseCubicIn(game->panelsAnimAmount));
			if (game->questionStr != nullptr)
			{
				RcNewDrawString(NtStr(game->questionStr), game->questionTextPos, questionColor, game->questionFlow.maxWidth);
			}
			
			r32 answerBackFade = answerShowing ? EaseCubicOut(game->answerAnimAmount) : EaseCubicIn(game->answerAnimAmount);
			RcEnableVignette(0.6f, 0.7f);
			RcDrawRectangle(game->questionRec, ColorTransparent(NewColor(game->answeredCorrectly ? Color_Green : Color_Red), answerBackFade));
			RcDisableVignette();
			
			RcBindNewFont(&app->questionFont);
			Color_t answerColor = ColorTransparent(ScoreColor, answerShowing ? EaseCubicOut(game->answerAnimAmount) : EaseCubicIn(game->answerAnimAmount));
			if (game->answerStr != nullptr)
			{
				RcNewDrawString(NtStr(game->answerStr), game->questionTextPos, answerColor, game->questionFlow.maxWidth);
			}
			
			RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
		}
		
		// +==============================+
		// |          Clue Board          |
		// +==============================+
		if (game->clueBoardAnimAmount > 0)
		{
			RcDrawRectangle(game->clueBoardRec, TrayColor);
			RcDrawGradient(NewRec(game->clueBoardRec.x, game->clueBoardRec.y, game->clueBoardRec.width, game->clueBoardRec.height/8), TrayColorLight, TrayColor, Dir2_Down);
			RcDrawGradient(NewRec(game->clueBoardRec.x, game->clueBoardRec.y + game->clueBoardRec.height*7/8, game->clueBoardRec.width, game->clueBoardRec.height/8), TrayColor, TrayColorDark, Dir2_Down);
			
			for (u32 cIndex = 0; cIndex < R2_NUM_CLUES_PER_Q; cIndex++)
			{
				RcEnableVignette(0.8f, 0.8f);
				RcDrawGradient(game->clueRecs[cIndex], ClueBackColor1, ClueBackColor2, Dir2_Down);
				RcDisableVignette();
				
				RcSetViewport(game->clueRecs[cIndex]);
				
				const char* clueStr = game->r2Clues[(game->questionNum*R2_NUM_CLUES_PER_Q) + cIndex];
				if (game->roundNum == 2 && game->showBoard && clueStr != nullptr && game->clueAnimAmounts[cIndex] > 0.0f)
				{
					RcBindNewFont(&app->clueFont);
					RcSetFontStyle(FontStyle_Default);
					RcNewDrawNtString(clueStr, game->clueTextPos[cIndex], ScoreColor, game->clueFlows[cIndex].maxWidth);
				}
				
				if (game->clueAnimAmounts[cIndex] < 1.0f)
				{
					RcEnableVignette(0.8f, 0.8f);
					RcDrawGradient(game->clueCoverRecs[cIndex], ClueCoverColor1, ClueCoverColor2, Dir2_Down);
					RcDisableVignette();
					
					RcBindNewFont(&app->clueNumberFont);
					RcNewPrintString(game->clueNumberTextPos[cIndex], ClueNumberColor, "%u", cIndex+1);
				}
				
				RcDrawButton(game->clueRecs[cIndex], ColorTransparent(0), NewColor(Color_Black), 1);
				
				RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
			}
		}
		
		// +==============================+
		// |       Draw Round Title       |
		// +==============================+
		if (game->roundNum >= 1 && game->roundNum <= 3)
		{
			RcBindNewFont(&app->clueFont);
			RcNewPrintString(NewVec2(RenderScreenSize.width/2, clueMaxExtendUp+5), ScoreColor, "Round %u", game->roundNum);
		}
		
		// +==============================+
		// |       Draw Team Names        |
		// +==============================+
		{
			RcBindNewFont(&app->clueFont);
			RcNewPrintString(NewVec2(RenderScreenSize.width*1/4, clueMaxExtendUp+5), LeftColor, "%s", (app->leftTeamName != nullptr) ? app->leftTeamName : "Orange Team");
			RcNewPrintString(NewVec2(RenderScreenSize.width*3/4, clueMaxExtendUp+5), RightColor, "%s", (app->rightTeamName != nullptr) ? app->rightTeamName : "Blue Team");
		}
		
		// +==============================+
		// |        Draw Win Text         |
		// +==============================+
		if (game->winTextAnimAmount > 0)
		{
			char* winTextStr = TempPrint("%s Wins!", game->rightTeamWins ? app->rightTeamName : app->leftTeamName);
			v2 winTextSize = FontMeasureNtString(winTextStr, &app->scoreFont);
			v2 textPos = RenderScreenSize/2;
			textPos.x += (RenderScreenSize.width/2 + winTextSize.width/2) * Ease(EasingStyle_QuadraticIn, 1-game->winTextAnimAmount);
			RcBindNewFont(&app->scoreFont);
			RcNewDrawNtString(winTextStr, textPos, ScoreColor);
		}
		
		// +==============================+
		// |        Volume Meters         |
		// +==============================+
		if (game->volumeChangeTime < VOLUMES_SHOW_TIME)
		{
			rec wholeRec = NewRec(0, 0, 20, 300);
			rec fillRec = wholeRec;
			fillRec.height *= app->soundsVolume;
			fillRec.y = wholeRec.y + wholeRec.height - fillRec.height;
			RcDrawRectangle(wholeRec, NewColor(Color_LightGrey));
			RcDrawRectangle(fillRec, NewColor(Color_Orange));
			RcDrawButton(wholeRec, NewColor(Color_TransparentBlack), NewColor(Color_Black), 1);
		}
		if (game->volumeChangeTime < VOLUMES_SHOW_TIME)
		{
			rec wholeRec = NewRec(20, 0, 20, 300);
			rec fillRec = wholeRec;
			fillRec.height *= app->musicVolume;
			fillRec.y = wholeRec.y + wholeRec.height - fillRec.height;
			RcDrawRectangle(wholeRec, NewColor(Color_LightGrey));
			RcDrawRectangle(fillRec, NewColor(Color_Orange));
			RcDrawButton(wholeRec, NewColor(Color_TransparentBlack), NewColor(Color_Black), 1);
		}
		
		// RcNewPrintString(NewVec2(10, 20), ScoreColor, "%lu", platform->timeElapsed);
	}
}


